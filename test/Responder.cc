/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../src/Responder.hh"

using namespace std;
using namespace testing;
using namespace Sc2Ai;

namespace {
	class MockResponder :
		public Responder
	{
		public:
			MOCK_METHOD(void, onError, (decltype(declval<Response>().error()) const &), (override));
			MOCK_METHOD(void, onStateChange, (Status const &), (override));
			MOCK_METHOD(void, onCreateGame, (ResponseCreateGame const &), (override));
			MOCK_METHOD(void, onJoinGame, (ResponseJoinGame const &), (override));
			MOCK_METHOD(void, onRestartGame, (ResponseRestartGame const &), (override));
			MOCK_METHOD(void, onStartReplay, (ResponseStartReplay const &), (override));
			MOCK_METHOD(void, onLeaveGame, (ResponseLeaveGame const &), (override));
			MOCK_METHOD(void, onQuickSave, (ResponseQuickSave const &), (override));
			MOCK_METHOD(void, onQuickLoad, (ResponseQuickLoad const &), (override));
			MOCK_METHOD(void, onQuit, (ResponseQuit const &), (override));
			MOCK_METHOD(void, onGameInfo, (ResponseGameInfo const &), (override));
			MOCK_METHOD(void, onObservation, (ResponseObservation const &), (override));
			MOCK_METHOD(void, onAction, (ResponseAction const &), (override));
			MOCK_METHOD(void, onObserverAction, (ResponseObserverAction const &), (override));
			MOCK_METHOD(void, onStep, (ResponseStep const &), (override));
			MOCK_METHOD(void, onData, (ResponseData const &), (override));
			MOCK_METHOD(void, onQuery, (ResponseQuery const &), (override));
			MOCK_METHOD(void, onSaveReplay, (ResponseSaveReplay const &), (override));
			MOCK_METHOD(void, onReplayInfo, (ResponseReplayInfo const &), (override));
			MOCK_METHOD(void, onAvailableMaps, (ResponseAvailableMaps const &), (override));
			MOCK_METHOD(void, onSaveMap, (ResponseSaveMap const &), (override));
			MOCK_METHOD(void, onMapCommand, (ResponseMapCommand const &), (override));
			MOCK_METHOD(void, onPing, (ResponsePing const &), (override));
			MOCK_METHOD(void, onDebug, (ResponseDebug const &), (override));
	};

	TEST(ResponderTests, Error) {
		using SC2APIProtocol::unknown;

		auto mock = StrictMock<MockResponder>{};
		auto res = Response{};
		res.set_status(unknown);

		EXPECT_CALL(mock, onError(_))
			.Times(1);

		mock.onResponse(res);
	}

	TEST(ResponderTests, StateChange) {
		using SC2APIProtocol::launched;
		using SC2APIProtocol::ended;
		using SC2APIProtocol::quit;

		auto mock = StrictMock<MockResponder>{};
		auto res = Response{};
		res.mutable_create_game();

		EXPECT_CALL(mock, onCreateGame(_))
			.Times(5);

		EXPECT_CALL(mock, onStateChange(launched))
			.Times(1);

		EXPECT_CALL(mock, onStateChange(ended))
			.Times(1);

		EXPECT_CALL(mock, onStateChange(quit))
			.Times(1);

		res.set_status(launched);
		mock.onResponse(res);

		res.set_status(ended);
		mock.onResponse(res);
		mock.onResponse(res);
		mock.onResponse(res);

		res.set_status(quit);
		mock.onResponse(res);
	}

#	define CALLTEST(a, b) TEST(ResponderTests, a) { \
		auto mock = StrictMock<MockResponder>{}; auto res = Response{}; res.set_status(SC2APIProtocol::unknown); \
		res.mutable_ ## b(); EXPECT_CALL(mock, on ## a(_)).Times(1); mock.onResponse(res); }

	CALLTEST(CreateGame, create_game)
	CALLTEST(JoinGame, join_game)
	CALLTEST(RestartGame, restart_game)
	CALLTEST(StartReplay, start_replay)
	CALLTEST(LeaveGame, leave_game)
	CALLTEST(QuickSave, quick_save)
	CALLTEST(QuickLoad, quick_load)
	CALLTEST(Quit, quit)
	CALLTEST(GameInfo, game_info)
	CALLTEST(Observation, observation)
	CALLTEST(Action, action)
	CALLTEST(ObserverAction, obs_action)
	CALLTEST(Step, step)
	CALLTEST(Data, data)
	CALLTEST(Query, query)
	CALLTEST(SaveReplay, save_replay)
	CALLTEST(ReplayInfo, replay_info)
	CALLTEST(AvailableMaps, available_maps)
	CALLTEST(SaveMap, save_map)
	CALLTEST(MapCommand, map_command)
	CALLTEST(Ping, ping)
	CALLTEST(Debug, debug)

}
