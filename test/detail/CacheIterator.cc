/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

// 2020-05-16 AMR NOTE: this entire file is an implementaiton detail, is there a better way to test?
#include <gtest/gtest.h>
#include "../../src/detail/CacheIterator.hh"

using namespace std;
using namespace testing;
using namespace Sc2Ai::detail;

namespace {
	TEST(detailCacheIteratorTests, Equal) {
		auto values = map<uint64_t, pair<uint64_t, uint64_t>>{
			{1u, {2u, 3u}},
			{4u, {5u, 6u}},
			{7u, {8u, 9u}},
		};

		auto a = CacheIterator<pair<uint64_t, uint64_t>>(values.begin());
		auto b = CacheIterator<pair<uint64_t, uint64_t>>(++values.begin());

		EXPECT_NE(a, b);
		EXPECT_NE(a++, b);
		EXPECT_EQ(a, b);
	}

	TEST(detailCacheIteratorTests, Dereference) {
		auto values = map<uint64_t, pair<uint64_t, uint64_t>>{
			{1u, {2u, 3u}},
			{4u, {5u, 6u}},
			{7u, {8u, 9u}},
		};

		auto a = CacheIterator<pair<uint64_t, uint64_t>>(values.begin());

		EXPECT_EQ(a->first, (*a).first);
		EXPECT_EQ(a->first, 2u);
		EXPECT_EQ(a->second, 3u);
		EXPECT_EQ((++a)->second, 6u);
	}

	TEST(detailCacheIteratorTests, Decrement) {
		auto values = map<uint64_t, pair<uint64_t, uint64_t>>{
			{1u, {2u, 3u}},
			{4u, {5u, 6u}},
			{7u, {8u, 9u}},
		};

		auto a = CacheIterator<pair<uint64_t, uint64_t>>(values.begin());
		auto b = CacheIterator<pair<uint64_t, uint64_t>>(++values.begin());

		EXPECT_NE(a, b);
		EXPECT_EQ(a, --b);

		++a; ++b;

		EXPECT_EQ(a, b);
		EXPECT_EQ(a--, b);
		EXPECT_NE(a, b);
	}
}
