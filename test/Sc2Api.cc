/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../src/Sc2Api.hh"

using namespace std;
using namespace testing;
using namespace Sc2Ai;

namespace {
	class MockResponder :
		public Responder
	{
		public:
			MOCK_METHOD(void, onError, (decltype(declval<Response>().error()) const &), (override));
			MOCK_METHOD(void, onStateChange, (Status const &), (override));
			MOCK_METHOD(void, onCreateGame, (ResponseCreateGame const &), (override));
			MOCK_METHOD(void, onJoinGame, (ResponseJoinGame const &), (override));
			MOCK_METHOD(void, onRestartGame, (ResponseRestartGame const &), (override));
			MOCK_METHOD(void, onStartReplay, (ResponseStartReplay const &), (override));
			MOCK_METHOD(void, onLeaveGame, (ResponseLeaveGame const &), (override));
			MOCK_METHOD(void, onQuickSave, (ResponseQuickSave const &), (override));
			MOCK_METHOD(void, onQuickLoad, (ResponseQuickLoad const &), (override));
			MOCK_METHOD(void, onQuit, (ResponseQuit const &), (override));
			MOCK_METHOD(void, onGameInfo, (ResponseGameInfo const &), (override));
			MOCK_METHOD(void, onObservation, (ResponseObservation const &), (override));
			MOCK_METHOD(void, onAction, (ResponseAction const &), (override));
			MOCK_METHOD(void, onObserverAction, (ResponseObserverAction const &), (override));
			MOCK_METHOD(void, onStep, (ResponseStep const &), (override));
			MOCK_METHOD(void, onData, (ResponseData const &), (override));
			MOCK_METHOD(void, onQuery, (ResponseQuery const &), (override));
			MOCK_METHOD(void, onSaveReplay, (ResponseSaveReplay const &), (override));
			MOCK_METHOD(void, onReplayInfo, (ResponseReplayInfo const &), (override));
			MOCK_METHOD(void, onAvailableMaps, (ResponseAvailableMaps const &), (override));
			MOCK_METHOD(void, onSaveMap, (ResponseSaveMap const &), (override));
			MOCK_METHOD(void, onMapCommand, (ResponseMapCommand const &), (override));
			MOCK_METHOD(void, onPing, (ResponsePing const &), (override));
			MOCK_METHOD(void, onDebug, (ResponseDebug const &), (override));
	};

	class MockApi :
		public Sc2Api
	{
		public:
			MockApi(
				decltype(declval<MockApi>().io()) i,
				Responder &r,
				string const &u
			) :
				Sc2Api(i, r, u)
			{}
			void onResponse(Response &&r) {
				return Sc2Api::onResponse(move(r));
			}
	};

	class Sc2ApiTests :
		public Test
	{
		protected:
			net::io_context io;
			MockResponder res;
			std::shared_ptr<MockApi> ws;
	};

	TEST_F(Sc2ApiTests, Create) {
		using SC2APIProtocol::unknown;

		ws = make_shared<MockApi>(io, res, "http://localhost/"s);

		EXPECT_CALL(res, onError(_))
			.Times(1);

		auto r = Response{};
		r.set_status(unknown);

		ws->onResponse(move(r));
	}
}
