/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../modules/Supply.cc"

using namespace testing;

namespace {
	class ApiMock :
		public Sc2Api
	{
		public:
			ApiMock(net::io_context &i, Responder &r) :
				Sc2Api(i, r)
			{}
			MOCK_METHOD(void, send, (Request const &), (override));
	};

	class TrainMock :
		public Module
	{
		public:
			TrainMock() :
				Module("Train"s, {})
			{}
			MOCK_METHOD(any, defCall, (any const &), (override));
	};

	char const *PARAMS[] = {
		"SUPPLY TESTS"
	};

	class AppMock :
		public Application
	{
		public:
			AppMock() :
				Application(1, PARAMS)
			{}
			MOCK_METHOD(bool, load, (string const &), (override));
			MOCK_METHOD(void, modules, (function<void(Coeus::Import<Module> &)> const &), (override));
	};

	class SupplyTests :
		public Test,
		public Supply
	{
		private:
			Coeus::Import<Module> _train;
			net::io_context _io;
		protected:
			shared_ptr<StrictMock<AppMock>> app;
			shared_ptr<StrictMock<ApiMock>> api;
			shared_ptr<StrictMock<TrainMock>> train;
			void SetUp() override {
				auto res = ResponseJoinGame{};
				auto i = 42u;

				app = make_shared<decltype(app)::element_type>();
				api = make_shared<decltype(api)::element_type>(_io, *this);
				_train = train = make_shared<decltype(train)::element_type>();

				EXPECT_CALL(*app, modules(_))
					.WillOnce(InvokeArgument<0>(_train));

				EXPECT_CALL(*app, load(Eq("Train"s)))
					.WillOnce(Return(true));

				res.set_player_id(i);

				setApplication(app);
				setApi(api);
				onJoinGame(res);

				ASSERT_EQ(i, id());
			}
			void TearDown() override {
				api.reset();
				setApi(api);
			}
			void setRace(Race const &r) {
				auto res = Response{};
				auto inf = res.mutable_game_info();
				auto player = inf->add_player_info();

				inf->mutable_start_raw();

				player->set_player_id(id());
				player->set_race_actual(r);

				onResponse(res);
				ASSERT_EQ(r, race());
			}
			auto setValues(uint32_t const min, uint32_t const fcap, uint32_t const fused) -> void {
				auto player = PlayerCommon{};

				player.set_minerals(min);
				player.set_food_cap(fcap);
				player.set_food_used(fused);

				onPlayer(player);
			}
			auto setSupplyUnit(uint32_t const id) -> void{
				auto data = UnitTypeData{};

				data.set_available(true);
				data.set_race(race());
				data.set_name("Overlord"s);
				data.set_mineral_cost(100u);
				data.set_food_provided(8u);
				data.set_unit_id(id);

				onUnitTypeData(data);
			}
	};

	TEST_F(SupplyTests, PendingFoodZero) {
		EXPECT_EQ(pending(), 0u);
	}

	TEST_F(SupplyTests, GetCap) {
		auto v = 14u;
		setValues(50u, v, 12u);

		auto r = call();
		EXPECT_EQ(v, any_cast<uint32_t>(r));
	}

	TEST_F(SupplyTests, isAvailable) {
		auto unit = UnitTypeData{};
		EXPECT_FALSE(isAvailable(unit));

		unit.set_available(false);
		EXPECT_FALSE(isAvailable(unit));

		unit.set_available(true);
		EXPECT_TRUE(isAvailable(unit));
	}

	TEST_F(SupplyTests, isMyRace) {
		setRace(Zerg);

		auto unit = UnitTypeData{};
		EXPECT_FALSE(isMyRace(unit));

		unit.set_race(NoRace);
		EXPECT_FALSE(isMyRace(unit));

		unit.set_race(Zerg);
		EXPECT_TRUE(isMyRace(unit));

		unit.set_race(::SC2APIProtocol::Protoss);
		EXPECT_FALSE(isMyRace(unit));
	}

	TEST_F(SupplyTests, isMySupply) {
		setRace(Zerg);

		auto unit = UnitTypeData{};

		unit.set_available(true);
		unit.set_race(race());
		EXPECT_FALSE(isMySupply(unit));

		unit.set_name("Overlord"s);
		EXPECT_TRUE(isMySupply(unit));

		unit.set_name("SupplyDepot"s);
		EXPECT_TRUE(isMySupply(unit));

		unit.set_name("Pylon"s);
		EXPECT_TRUE(isMySupply(unit));

		unit.set_name("Drone"s);
		EXPECT_FALSE(isMySupply(unit));
	}

	TEST_F(SupplyTests, Pendings) {
	}
}
