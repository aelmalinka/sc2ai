/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../modules/Buildings.cc"

using namespace testing;

namespace {
	class WorkersMock :
		public Module
	{
		public:
			WorkersMock() :
				Module("Workers"s, {})
			{}
			MOCK_METHOD(any, defCall, (any const &), (override));
	};

	class ApiMock :
		public Sc2Api
	{
		public:
			ApiMock(net::io_context &i, Responder &r) :
				Sc2Api(i, r)
			{}
			MOCK_METHOD(void, send, (Request const &), (override));
	};

	char const *PARAMS[] {
		"BUILDINGS TESTS",
	};

	class AppMock :
		public Application
	{
		public:
			AppMock() :
				Application(1, PARAMS)
			{}
			MOCK_METHOD(bool, load, (string const &), (override));
			MOCK_METHOD(void, modules, (function<void(Coeus::Import<Module> &)> const &), (override));
	};

	class BuildingsTests :
		public Test,
		public Buildings
	{
		private:
			Coeus::Import<Module> _workers;
			net::io_context _io;
		protected:
			shared_ptr<StrictMock<WorkersMock>> workers;
			shared_ptr<StrictMock<ApiMock>> api;
			shared_ptr<StrictMock<AppMock>> app;
		protected:
			void SetUp() override {
				auto res = ResponseJoinGame{};
				auto i = 42u;

				api = make_shared<decltype(api)::element_type>(_io, *this);
				app = make_shared<decltype(app)::element_type>();
				_workers = workers = make_shared<decltype(workers)::element_type>();

				EXPECT_CALL(*app, load(Eq("Workers"s)))
					.WillOnce(Return(true));

				EXPECT_CALL(*app, modules(_))
					.WillOnce(InvokeArgument<0>(_workers));

				res.set_player_id(i);

				setApi(api);
				setApplication(app);
				onJoinGame(res);

				ASSERT_EQ(i, id());
			}
			void TearDown() override {
				api.reset();
				setApi(api);
			}
			void setRace(Race const &r) {
				auto res = ResponseGameInfo{};
				auto player = res.add_player_info();

				res.mutable_start_raw();

				player->set_player_id(id());
				player->set_race_actual(r);

				onGameInfo(res);
				ASSERT_EQ(r, race());
			}
	};

	TEST_F(BuildingsTests, isAvailable) {
		auto data = UnitTypeData{};
		EXPECT_FALSE(isAvailable(data));

		data.set_available(false);
		EXPECT_FALSE(isAvailable(data));

		data.set_available(true);
		EXPECT_TRUE(isAvailable(data));
	}

	TEST_F(BuildingsTests, isStructure) {
		auto data = UnitTypeData{};

		data.set_available(true);
		EXPECT_FALSE(isStructure(data));

		data.add_attributes(::SC2APIProtocol::Mechanical);
		EXPECT_FALSE(isStructure(data));

		data.add_attributes(Structure);
		EXPECT_TRUE(isStructure(data));

		data.add_attributes(::SC2APIProtocol::Massive);
		EXPECT_TRUE(isStructure(data));
	}

	TEST_F(BuildingsTests, isMyWorker) {
		auto id = 37ul;

		EXPECT_CALL(*workers, defCall(_))
			.WillOnce(Return(id));

		auto worker = getWorker();
		EXPECT_EQ(worker, id);

		auto unit = Unit{};
		unit.set_tag(id);
		EXPECT_TRUE(isMyWorker(unit));

		EXPECT_CALL(*workers, defCall(_))
			.WillOnce(Return(any{}));

		returnWorker(worker);
		EXPECT_FALSE(isMyWorker(unit));
	}
}
