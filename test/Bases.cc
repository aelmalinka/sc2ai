/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../modules/Bases.cc"

using namespace testing;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	class ApiMock :
		public Sc2Api
	{
		public:
			ApiMock(net::io_context &i, Responder &r) :
				Sc2Api(i, r)
			{}
			MOCK_METHOD(void, send, (Request const &), (override));
	};

	class BasesTests :
		public Test,
		public Bases
	{
		private:
			net::io_context _io;
		protected:
			shared_ptr<StrictMock<ApiMock>> api;
			void SetUp() override {
				auto res = ResponseJoinGame{};
				auto i = 42u;

				api = make_shared<decltype(api)::element_type>(_io, *this);

				res.set_player_id(i);

				setApi(api);
				onJoinGame(res);

				ASSERT_EQ(i, id());
			}
			void TearDown() override {
				api.reset();
				setApi(api);
			}
	};

	TEST_F(BasesTests, getBasesNone) {
		TEST_BEGIN
		auto d = call();
		auto t = any_cast<tuple<Cache<Unit>, list<tuple<float, float>>>>(d);
		auto b = get<0>(t);
		auto l = get<1>(t);

		EXPECT_EQ(b.size(), 0ul);
		EXPECT_TRUE(b.empty());

		EXPECT_EQ(l.size(), 0ul);
		EXPECT_TRUE(l.empty());
		TEST_END
	}

	TEST_F(BasesTests, isAvailable) {
		auto data = UnitTypeData{};
		EXPECT_FALSE(isAvailable(data));

		data.set_available(false);
		EXPECT_FALSE(isAvailable(data));

		data.set_available(true);
		EXPECT_TRUE(isAvailable(data));
	}

	TEST_F(BasesTests, isStructure) {
		auto data = UnitTypeData{};

		data.set_available(true);
		EXPECT_FALSE(isStructure(data));

		data.add_attributes(::SC2APIProtocol::Mechanical);
		EXPECT_FALSE(isStructure(data));

		data.add_attributes(Structure);
		EXPECT_TRUE(isStructure(data));

		data.add_attributes(::SC2APIProtocol::Massive);
		EXPECT_TRUE(isStructure(data));
	}

	TEST_F(BasesTests, isBasicBaseType) {
		auto data = UnitTypeData{};
		data.set_available(true);

		EXPECT_FALSE(isBasicBaseType(data));

		data.add_attributes(Structure);
		EXPECT_FALSE(isBasicBaseType(data));

		data.set_name(""s);
		EXPECT_FALSE(isBasicBaseType(data));

		data.set_name("CommandCenter"s);
		//EXPECT_TRUE(isBasicBaseType(data));

		// 2020-07-15 AMR NOTE: only supporting zerg for now (have to know which base to request)
		// 2020-07-15 AMR TODO: only store base matching race or what?
		data.set_name("Hatchery"s);
		EXPECT_TRUE(isBasicBaseType(data));

		data.set_name("Nexus"s);
		//EXPECT_TRUE(isBasicBaseType(data));

		data.set_name("Asdf"s);
		EXPECT_FALSE(isBasicBaseType(data));
	}

	TEST_F(BasesTests, isMyUnit) {
		auto data = Unit{};
		EXPECT_FALSE(isMyUnit(data));

		data.set_owner(id() + 1);
		EXPECT_FALSE(isMyUnit(data));

		data.set_owner(id());
		EXPECT_TRUE(isMyUnit(data));
	}

	TEST_F(BasesTests, isMyBase) {
		auto cc_id = 40u;
		auto hatch_id = 41u;
		auto nexus_id = 42u;
		auto data = UnitTypeData{};

		data.set_available(true);
		data.add_attributes(Structure);

		data.set_unit_id(cc_id);
		data.set_name("CommandCenter"s);
		//ASSERT_TRUE(isBasicBaseType(data));
		onUnitTypeData(data);

		data.set_unit_id(hatch_id);
		data.set_name("Hatchery"s);
		ASSERT_TRUE(isBasicBaseType(data));
		onUnitTypeData(data);

		data.set_unit_id(nexus_id);
		data.set_name("Nexus"s);
		//ASSERT_TRUE(isBasicBaseType(data));
		onUnitTypeData(data);

		auto unit = Unit{};

		unit.set_owner(id());
		EXPECT_FALSE(isMyBase(unit));

		unit.set_unit_type(cc_id);
		//EXPECT_TRUE(isMyBase(unit));

		unit.set_unit_type(hatch_id);
		EXPECT_TRUE(isMyBase(unit));

		unit.set_unit_type(nexus_id);
		//EXPECT_TRUE(isMyBase(unit));

		unit.set_unit_type(nexus_id + 3);
		EXPECT_FALSE(isMyBase(unit));
	}
}
