/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../modules/Train.cc"

using namespace testing;

namespace {
	class ApiMock :
		public Sc2Api
	{
		public:
			ApiMock(net::io_context &i, Responder &r) :
				Sc2Api(i, r)
			{}
			MOCK_METHOD(void, send, (Request const &), (override));
	};

	class TrainTests :
		public Test,
		public Train
	{
		private:
			net::io_context _io;
		protected:
			shared_ptr<StrictMock<ApiMock>> api;
			void SetUp() override {
				auto res = ResponseJoinGame{};
				auto i = 42u;

				api = make_shared<decltype(api)::element_type>(_io, *this);

				res.set_player_id(i);

				setApi(api);
				onJoinGame(res);

				ASSERT_EQ(i, id());
			}
			void TearDown() override {
				api.reset();
				setApi(api);
			}
			void setRace(Race const &r) {
				auto res = Response{};
				auto inf = res.mutable_game_info();
				auto player = inf->add_player_info();

				inf->mutable_start_raw();

				player->set_player_id(id());
				player->set_race_actual(r);

				onResponse(res);

				ASSERT_EQ(r, race());
			}
	};

	TEST_F(TrainTests, EmptyCall) {
		auto a = call();

		EXPECT_FALSE(a.has_value());
	}

	TEST_F(TrainTests, FailedQueue) {
		auto a = call(42u);

		EXPECT_FALSE(any_cast<bool>(a));
	}

	TEST_F(TrainTests, TrainingGroundTypeZerg) {
		setRace(Zerg);

		auto a = UnitTypeData{};

		EXPECT_FALSE(isTrainingGroundType(a));

		a.set_name("Larva"s);
		EXPECT_TRUE(isTrainingGroundType(a));
	}

	TEST_F(TrainTests, TrainingGroundZerg) {
		setRace(Zerg);

		auto id = 42u;
		auto a = UnitTypeData{};
		auto b = Unit{};

		a.set_unit_id(id);
		b.set_unit_type(id);
		a.set_name("Larva"s);

		EXPECT_TRUE(isTrainingGroundType(a));

		onUnitTypeData(a);
		EXPECT_TRUE(isTrainingGround(b));
	}

	TEST_F(TrainTests, TrainUnit) {
		setRace(Zerg);

		auto id_type = 42u;
		auto id_where = 43u;
		auto a = UnitTypeData{};
		auto b = Unit{};

		a.set_unit_id(id_type);
		a.set_name("Larva"s);

		b.set_tag(id_where);
		b.set_unit_type(id_type);

		onUnitTypeData(a);
		onUnit(b);

		auto c = call(id_type);

		EXPECT_TRUE(any_cast<bool>(c));
	}
}
