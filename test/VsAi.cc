/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../modules/VsAi.cc"

using namespace testing;

namespace {
	class ApiMock :
		public Sc2Api
	{
		public:
			ApiMock(net::io_context &i, Responder &r) :
				Sc2Api(i, r)
			{}
			MOCK_METHOD(void, send, (Request const &), (override));
	};

	class VsAiTests :
		public Test,
		public VsAi
	{
		private:
			net::io_context _io;
		protected:
			bool save;
			shared_ptr<StrictMock<ApiMock>> api;
			void SetUp() override {
				save = false;
				api = make_shared<StrictMock<ApiMock>>(_io, *this);
				setApi(api);
			}
			void TearDown() override {
				// 2020-05-10 AMR NOTE: clean api before destructing _io
				api.reset();
				setApi(api);
			}
			void saveReplay(path const &a, string const &b) {
				if(save)
					VsAi::saveReplay(a, b);
			}
	};

	TEST_F(VsAiTests, CreateGame) {
		auto req = RequestCreateGame{};

		requestCreateGame(&req);

		EXPECT_EQ(req.player_setup(0).type(), Participant);
		EXPECT_EQ(req.player_setup(0).player_name(), my_name);
		EXPECT_EQ(req.player_setup(0).race(), my_race);

		EXPECT_EQ(req.player_setup(1).type(), Computer);
		EXPECT_EQ(req.player_setup(1).difficulty(), op_diff);
		EXPECT_EQ(req.player_setup(1).race(), op_race);
		EXPECT_EQ(req.player_setup(1).ai_build(), aibuild);
	}

	TEST_F(VsAiTests, JoinGame) {
		auto req = RequestJoinGame{};
		auto res = ResponseCreateGame{};

		requestJoinGame(res, &req);

		EXPECT_EQ(req.race(), my_race);
		EXPECT_TRUE(req.options().raw());
		EXPECT_TRUE(req.options().score());
		EXPECT_FALSE(req.options().raw_affects_selection());
	}

	TEST_F(VsAiTests, Done) {
		auto req = Request{};

		EXPECT_CALL(*api, send(_))
			.WillOnce(SaveArg<0>(&req));

		onDone();

		EXPECT_TRUE(req.has_save_replay());
	}

	TEST_F(VsAiTests, ResponseSaveReplay) {
		auto req = Request{};
		auto res = ResponseSaveReplay{};

		EXPECT_CALL(*api, send(_))
			.WillOnce(SaveArg<0>(&req));

		onSaveReplay(res);

		EXPECT_TRUE(req.has_quit());
	}

	TEST_F(VsAiTests, SaveReplay) {
		auto where = path("test"s);
		auto data = "asdf"s;

		save = true;
		saveReplay(where, data);

		auto lines = ""s;
		auto fh = ifstream(where.string());

		while(fh && !fh.eof()) {
			auto l = ""s;
			getline(fh, l);
			lines += l;
		}

		EXPECT_TRUE(exists(path("test"s)));
		EXPECT_EQ(lines, data);

		EXPECT_TRUE(remove(where));
	}
}
