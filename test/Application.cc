/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Application.hh"

using namespace std;
using namespace testing;
using namespace Sc2Ai;

namespace {
	TEST(ApplicationTests, Data) {
		auto params = vector<char const *>{
			"APPLICATION_TESTS",
		};

		auto app = Application(params.size(), params.data());

		app.data()->Abilities();
		app.data()->Upgrades();

		auto req = RequestData{};

		app.data()->requestData(&req);

		EXPECT_TRUE(req.has_ability_id());
		EXPECT_FALSE(req.has_unit_type_id());
		EXPECT_TRUE(req.has_upgrade_id());
		EXPECT_FALSE(req.has_buff_id());
		EXPECT_FALSE(req.has_effect_id());

		EXPECT_TRUE(req.ability_id());
		EXPECT_TRUE(req.upgrade_id());
	}
}
