/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include "../src/Uri.hh"

using namespace std;
using namespace testing;
using namespace Sc2Ai;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	TEST(UriTests, Http) {
		TEST_BEGIN
		auto uri = Uri("http://localhost/"s);

		ASSERT_TRUE(uri.hasAuthority());
		ASSERT_FALSE(uri.hasQuery());
		ASSERT_FALSE(uri.hasFragment());

		ASSERT_FALSE(uri.Auth().hasPort());
		ASSERT_FALSE(uri.Auth().hasUser());

		EXPECT_EQ(uri.Scheme(), "http"s);
		EXPECT_EQ(uri.Path(), "/"s);
		EXPECT_EQ(uri.Auth().Host(), "localhost"s);

		EXPECT_EQ(static_cast<string>(uri), "http://localhost/"s);
		TEST_END
	}

	TEST(UriTests, HttpPort) {
		TEST_BEGIN
		auto uri = Uri("http://localhost:8080/"s);

		ASSERT_TRUE(uri.hasAuthority());
		ASSERT_FALSE(uri.hasQuery());
		ASSERT_FALSE(uri.hasFragment());

		ASSERT_TRUE(uri.Auth().hasPort());
		ASSERT_FALSE(uri.Auth().hasUser());

		EXPECT_EQ(uri.Scheme(), "http"s);
		EXPECT_EQ(uri.Path(), "/"s);
		EXPECT_EQ(uri.Auth().Host(), "localhost"s);
		EXPECT_EQ(uri.Auth().Port(), "8080"s);

		EXPECT_EQ(static_cast<string>(uri), "http://localhost:8080/"s);
		TEST_END
	}

	TEST(UriTests, HttpQueryFragment) {
		TEST_BEGIN
		auto uri = Uri("http://localhost/?asdf#jkl"s);

		ASSERT_TRUE(uri.hasAuthority());
		ASSERT_TRUE(uri.hasQuery());
		ASSERT_TRUE(uri.hasFragment());

		ASSERT_FALSE(uri.Auth().hasPort());
		ASSERT_FALSE(uri.Auth().hasUser());

		EXPECT_EQ(uri.Scheme(), "http"s);
		EXPECT_EQ(uri.Path(), "/"s);
		EXPECT_EQ(uri.Auth().Host(), "localhost"s);
		EXPECT_EQ(uri.Query(), "asdf"s);
		EXPECT_EQ(uri.Fragment(), "jkl"s);

		EXPECT_EQ(static_cast<string>(uri), "http://localhost/?asdf#jkl"s);
		TEST_END
	}

	TEST(UriTests, Https) {
		TEST_BEGIN
		auto uri = Uri("https://localhost/"s);

		ASSERT_TRUE(uri.hasAuthority());
		ASSERT_FALSE(uri.hasQuery());
		ASSERT_FALSE(uri.hasFragment());

		ASSERT_FALSE(uri.Auth().hasPort());
		ASSERT_FALSE(uri.Auth().hasUser());

		EXPECT_EQ(uri.Scheme(), "https"s);
		EXPECT_EQ(uri.Path(), "/"s);
		EXPECT_EQ(uri.Auth().Host(), "localhost"s);

		EXPECT_EQ(static_cast<string>(uri), "https://localhost/"s);
		TEST_END
	}

	TEST(UriTests, HttpsPort) {
		TEST_BEGIN
		auto uri = Uri("https://localhost:8080/"s);

		ASSERT_TRUE(uri.hasAuthority());
		ASSERT_FALSE(uri.hasQuery());
		ASSERT_FALSE(uri.hasFragment());

		ASSERT_TRUE(uri.Auth().hasPort());
		ASSERT_FALSE(uri.Auth().hasUser());

		EXPECT_EQ(uri.Scheme(), "https"s);
		EXPECT_EQ(uri.Path(), "/"s);
		EXPECT_EQ(uri.Auth().Host(), "localhost"s);
		EXPECT_EQ(uri.Auth().Port(), "8080"s);

		EXPECT_EQ(static_cast<string>(uri), "https://localhost:8080/"s);
		TEST_END
	}

	TEST(UriTests, Mailto) {
		TEST_BEGIN
		auto uri = Uri("mailto:admin@entropy-development.com"s);

		ASSERT_FALSE(uri.hasAuthority());
		ASSERT_FALSE(uri.hasQuery());
		ASSERT_FALSE(uri.hasFragment());

		EXPECT_EQ(uri.Scheme(), "mailto"s);
		EXPECT_EQ(uri.Path(), "admin@entropy-development.com"s);

		EXPECT_EQ(static_cast<string>(uri), "mailto:admin@entropy-development.com"s);
		TEST_END
	}
}
