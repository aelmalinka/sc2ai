/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../src/Observer.hh"

using namespace std;
using namespace Sc2Ai;

using testing::StrictMock;
using testing::_;

namespace {
	class MockObserver :
		public Observer
	{
		public:
			MockObserver() :
				Observer("ObserverTests"s, {})
			{}
			uint32_t id() const {
				return Observer::id();
			}
			MOCK_METHOD(void, onAction, (Action const &), (override));
			MOCK_METHOD(void, onActionError, (ActionError const &), (override));
			MOCK_METHOD(void, onObservationMock, (Observation const &), ());
			MOCK_METHOD(void, onPlayerResult, (PlayerResult const &), (override));
			MOCK_METHOD(void, onChatReceived, (ChatReceived const &), (override));
			MOCK_METHOD(void, onPlayer, (PlayerCommon const &), (override));
			MOCK_METHOD(void, onAbilityData, (AbilityData const &), (override));
			MOCK_METHOD(void, onUnitTypeData, (UnitTypeData const &), (override));
			MOCK_METHOD(void, onUpgradeData, (UpgradeData const &), (override));
			MOCK_METHOD(void, onBuffData, (BuffData const &), (override));
			MOCK_METHOD(void, onEffectData, (EffectData const &), (override));
			void onObservation(Observation const &obs) override {
				Observer::onObservation(obs);
				onObservationMock(obs);
			}
	};

	TEST(ObserverTests, Id) {
		auto id = 42u;
		StrictMock<MockObserver> mock;

		auto res = Response{};
		auto msg = res.mutable_join_game();
		msg->set_player_id(id);

		mock.onResponse(res);

		EXPECT_EQ(mock.id(), id);
	}

	TEST(ObserverTests, onData) {
		StrictMock<MockObserver> mock;

		auto res = Response{};
		auto data = res.mutable_data();

		data->add_abilities();
		data->add_abilities();
		data->add_units();
		data->add_upgrades();
		data->add_upgrades();
		data->add_upgrades();
		data->add_buffs();
		data->add_effects();

		EXPECT_CALL(mock, onUnitTypeData(_));
		EXPECT_CALL(mock, onBuffData(_));
		EXPECT_CALL(mock, onEffectData(_));
		EXPECT_CALL(mock, onAbilityData(_))
			.Times(2);
		EXPECT_CALL(mock, onUpgradeData(_))
			.Times(3);

		mock.onResponse(res);
	}

	TEST(ObserverTests, Observation) {
		StrictMock<MockObserver> mock;

		EXPECT_CALL(mock, onObservationMock(_));

		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		auto obs = res.mutable_observation();
		obs->mutable_observation();

		mock.onResponse(start);
		mock.onResponse(res);
	}

	TEST(ObserverTests, ActionTwice) {
		StrictMock<MockObserver> mock;

		EXPECT_CALL(mock, onAction(_))
			.Times(2);

		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		auto obs = res.mutable_observation();
		obs->add_actions();
		obs->add_actions();

		mock.onResponse(start);
		mock.onResponse(res);
	}

	TEST(ObserverTests, ActionError) {
		StrictMock<MockObserver> mock;

		EXPECT_CALL(mock, onActionError(_));

		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		auto obs = res.mutable_observation();
		obs->add_action_errors();

		mock.onResponse(start);
		mock.onResponse(res);
	}

	TEST(ObserverTests, PlayerResult) {
		StrictMock<MockObserver> mock;

		EXPECT_CALL(mock, onPlayerResult(_));

		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		auto obs = res.mutable_observation();
		obs->add_player_result();

		mock.onResponse(start);
		mock.onResponse(res);
	}

	TEST(ObserverTests, PlayerCommon) {
		StrictMock<MockObserver> mock;

		EXPECT_CALL(mock, onObservationMock(_));
		EXPECT_CALL(mock, onPlayer(_));

		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		res.mutable_observation()->mutable_observation()->mutable_player_common();

		mock.onResponse(start);
		mock.onResponse(res);
	}

	TEST(ObserverTests, All) {
		StrictMock<MockObserver> mock;

		EXPECT_CALL(mock, onAction(_))
			.Times(2);
		EXPECT_CALL(mock, onActionError(_));
		EXPECT_CALL(mock, onObservationMock(_));
		EXPECT_CALL(mock, onPlayerResult(_));
		EXPECT_CALL(mock, onChatReceived(_))
			.Times(3);

		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		auto obs = res.mutable_observation();
		obs->add_actions();
		obs->add_actions();
		obs->add_action_errors();
		obs->mutable_observation();
		obs->add_player_result();
		obs->add_chat();
		obs->add_chat();
		obs->add_chat();

		mock.onResponse(start);
		mock.onResponse(res);
	}
}
