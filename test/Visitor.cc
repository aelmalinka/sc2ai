/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../src/Visitor.hh"

using namespace std;
using namespace testing;
using namespace Sc2Ai;

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e; }

namespace {
	class MockVisitor
	{
		public:
			MockVisitor(Visitor &a)
			{
				a.setDefault(bind(&MockVisitor::def, this, placeholders::_1));
				a.add<void>(bind(&MockVisitor::noparams, this));
				a.add<int>(bind(&MockVisitor::oneint, this, placeholders::_1));
				a.add<string>(bind(&MockVisitor::onestring, this, placeholders::_1));
				a.add<string, int, string>(bind(&MockVisitor::stringintstring, this, placeholders::_1, placeholders::_2, placeholders::_3));
				a.add<string, int>(bind(&MockVisitor::voidstringint, this, placeholders::_1, placeholders::_2));
				a.add<int, int>(bind(&MockVisitor::sometimesint, this, placeholders::_1, placeholders::_2));
				a.add<function<void(int const)>>(bind(&MockVisitor::callback, this, placeholders::_1));
				a.add<function<void(int const)>, int>(bind(&MockVisitor::callback, this, placeholders::_1));
			}
			MOCK_METHOD(std::any, def, (std::any const &), ());
			MOCK_METHOD(void, noparams, (), ());
			MOCK_METHOD(void, oneint, (int const), ());
			MOCK_METHOD(string, onestring, (string const &), ());
			MOCK_METHOD(int, stringintstring, (string const &, int const, string const &), ());
			MOCK_METHOD(void, voidstringint, (string const &, int const), ());
			MOCK_METHOD(any, sometimesint, (int const, int const), ());
			MOCK_METHOD(void, callback, (function<void(int const)> const &), ());
			MOCK_METHOD(void, callbackwithint, (function<void(int const)> const &, int const), ());
	};

	class MockedVisitorTests :
		public Test
	{
		private:
			Visitor visitor;
		protected:
			shared_ptr<StrictMock<MockVisitor>> mock;
			template<typename ...Args>
			auto call(Args && ...args) -> any {
				return visitor(std::forward<Args>(args)...);
			}
		public:
			void SetUp() override {
				mock = make_shared<decltype(mock)::element_type>(visitor);
			}
	};

	TEST(BasicVisitorTests, CreateEmpty) {
		TEST_BEGIN
			Visitor a;

			EXPECT_THROW(a(), Exception);
		TEST_END
	}

	TEST(BasicVisitorTests, CreateDefault) {
		TEST_BEGIN
			Visitor a([](auto const &) -> any {
				return {};
			});

			EXPECT_NO_THROW(a());
		TEST_END
	}

	TEST_F(MockedVisitorTests, Default) {
		TEST_BEGIN
			EXPECT_CALL(*mock, def(_))
				.WillOnce(Return(any{}));
			EXPECT_FALSE(call(mock).has_value());
		TEST_END
	}

	TEST_F(MockedVisitorTests, NoParams) {
		TEST_BEGIN
			EXPECT_CALL(*mock, noparams());
			EXPECT_FALSE(call().has_value());
		TEST_END
	}

	TEST_F(MockedVisitorTests, OneInt) {
		TEST_BEGIN
			auto val = 42;
			EXPECT_CALL(*mock, oneint(Eq(42)));
			EXPECT_FALSE(call(val).has_value());
		TEST_END
	}

	TEST_F(MockedVisitorTests, OneIntAny) {
		TEST_BEGIN
			auto val = any(42);
			EXPECT_CALL(*mock, oneint(_));
			EXPECT_FALSE(call(val).has_value());
		TEST_END
	}

	TEST_F(MockedVisitorTests, OneString) {
		TEST_BEGIN
			auto s = "Hello World"s;
			EXPECT_CALL(*mock, onestring(Eq(s)))
				.WillOnce(ReturnArg<0>());

			auto r = call(s);

			ASSERT_TRUE(r.has_value());
			ASSERT_NO_THROW(any_cast<decltype(s)>(r));
			EXPECT_EQ(any_cast<decltype(s)>(r), s);
		TEST_END
	}

	TEST_F(MockedVisitorTests, OneStringAny) {
		TEST_BEGIN
			auto s = any("Hello World"s);
			EXPECT_CALL(*mock, onestring(_))
				.WillOnce(ReturnArg<0>());

			auto r = call(s);

			ASSERT_TRUE(r.has_value());
			ASSERT_NO_THROW(any_cast<string>(r));
			EXPECT_EQ(any_cast<string>(r), any_cast<string>(s));
		TEST_END
	}

	TEST_F(MockedVisitorTests, StringIntString) {
		TEST_BEGIN
			auto s1 = "Hello"s;
			auto s2 = "World"s;
			auto v = 42;

			EXPECT_CALL(*mock, stringintstring(Eq(s1), Eq(v), Eq(s2)))
				.WillOnce(ReturnArg<1>());

			auto r = call(s1, v, s2);

			ASSERT_TRUE(r.has_value());
			ASSERT_NO_THROW(any_cast<decltype(v)>(r));
			EXPECT_EQ(any_cast<decltype(v)>(r), v);
		TEST_END
	}

	TEST_F(MockedVisitorTests, VoidStringInt) {
		TEST_BEGIN
			auto s = "Hello"s;
			auto v = 42;

			EXPECT_CALL(*mock, voidstringint(Eq(s), Eq(v)));
			EXPECT_FALSE(call(s, v).has_value());
		TEST_END
	}

	TEST_F(MockedVisitorTests, SometimesInt) {
		TEST_BEGIN
			auto v = 32;
			auto p1 = 1;
			auto p2 = 2;
			EXPECT_CALL(*mock, sometimesint(Eq(p1), Eq(p2)))
				.WillOnce(Return(v));

			auto r = call(p1, p2);

			ASSERT_TRUE(r.has_value());
			ASSERT_NO_THROW(any_cast<decltype(v)>(r));
			EXPECT_EQ(any_cast<decltype(v)>(r), v);

			EXPECT_CALL(*mock, sometimesint(Eq(p1), Eq(p2)))
				.WillOnce(Return(any{}));
			EXPECT_FALSE(call(p1, p2).has_value());
		TEST_END
	}

	TEST_F(MockedVisitorTests, DISABLED_Callback) {
		TEST_BEGIN
			auto val = 42;
			auto ran = false;
			EXPECT_CALL(*mock, callback(_))
				.WillOnce(InvokeArgument<0>(val));

			EXPECT_FALSE(call([&ran, &val](auto const v) {
				EXPECT_EQ(v, val);
				ran = true;
			}).has_value());

			EXPECT_TRUE(ran);
		TEST_END
	}

	TEST_F(MockedVisitorTests, DISABLED_CallbackWithInt) {
		TEST_BEGIN
			auto val = 42;
			auto ran = false;
			EXPECT_CALL(*mock, callbackwithint(_, Eq(val)))
				.WillOnce(InvokeArgument<0>(val));

			EXPECT_FALSE(call([&ran, &val](auto const v) {
				EXPECT_EQ(v, val);
				ran = true;
			}, val).has_value());

			EXPECT_TRUE(ran);
		TEST_END
	}
}
