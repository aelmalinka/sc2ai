/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../modules/Workers.cc"

using namespace testing;

namespace {
	class BasesMock :
		public Module
	{
		public:
			BasesMock() :
				Module("Bases"s, {})
			{}
			MOCK_METHOD(any, defCall, (any const &), (override));
	};

	class TrainMock :
		public Module
	{
		public:
			TrainMock() :
				Module("Train"s, {})
			{}
			MOCK_METHOD(any, defCall, (any const &), (override));
	};

	class SupplyMock :
		public Module
	{
		public:
			SupplyMock() :
				Module("Supply"s, {})
			{}
			MOCK_METHOD(any, defCall, (any const &), (override));
	};

	char const *PARAMS[] = {
		"WORKERS TESTS"
	};

	class AppMock :
		public Application
	{
		public:
			AppMock() :
				Application(1, PARAMS)
			{}
			MOCK_METHOD(bool, load, (string const &), (override));
			MOCK_METHOD(void, modules, (function<void(Coeus::Import<Module> &)> const &), (override));
	};

	class ApiMock :
		public Sc2Api
	{
		public:
			ApiMock(net::io_context &i, Responder &r) :
				Sc2Api(i, r)
			{}
			MOCK_METHOD(void, send, (Request const &), (override));
	};

	class WorkersTests :
		public Test,
		public Workers
	{
		private:
			Coeus::Import<Module> _bases;
			Coeus::Import<Module> _train;
			Coeus::Import<Module> _supp;
			net::io_context _io;
			uint32_t _id;
		protected:
			shared_ptr<StrictMock<ApiMock>> api;
			shared_ptr<StrictMock<AppMock>> app;
			shared_ptr<StrictMock<BasesMock>> bases;
			shared_ptr<StrictMock<TrainMock>> train;
			shared_ptr<StrictMock<SupplyMock>> supp;
			void SetUp() override {
				_id = 42u;
				app = make_shared<StrictMock<AppMock>>();
				api = make_shared<StrictMock<ApiMock>>(_io, *this);
				_bases = bases = make_shared<StrictMock<BasesMock>>();
				_train = train = make_shared<StrictMock<TrainMock>>();
				_supp = supp = make_shared<StrictMock<SupplyMock>>();

				EXPECT_CALL(*app, modules(_))
					.WillOnce(DoAll(
						InvokeArgument<0>(_bases),
						InvokeArgument<0>(_train),
						InvokeArgument<0>(_supp)
					));

				EXPECT_CALL(*app, load(Eq("Train"s)))
					.WillOnce(Return(true));
				EXPECT_CALL(*app, load(Eq("Supply"s)))
					.WillOnce(Return(true));
				EXPECT_CALL(*app, load(Eq("Bases"s)))
					.WillOnce(Return(true));

				setApplication(app);
				setApi(api);

				auto res = ResponseJoinGame{};
				res.set_player_id(_id);

				onJoinGame(res);

				ASSERT_EQ(_id, id());
			}
			void TearDown() override {
				api.reset();
				setApi(api);
			}
			void setRace(Race const &r) {
				auto res = Response{};
				auto inf = res.mutable_game_info();
				auto player = inf->add_player_info();

				inf->mutable_start_raw();

				player->set_player_id(id());
				player->set_race_actual(r);

				onResponse(res);
				ASSERT_EQ(r, race());
			}
	};

	TEST_F(WorkersTests, Race) {
		auto r = ::SC2APIProtocol::Protoss;

		auto inf = ResponseGameInfo{};
		auto player = inf.add_player_info();

		inf.mutable_start_raw();

		player->set_race_actual(r);
		player->set_player_id(id());

		onGameInfo(inf);

		EXPECT_EQ(r, race());
	}

	TEST_F(WorkersTests, isAvailable) {
		auto unit = UnitTypeData{};

		EXPECT_FALSE(isAvailable(unit));

		unit.set_available(false);
		EXPECT_FALSE(isAvailable(unit));

		unit.set_available(true);
		EXPECT_TRUE(isAvailable(unit));
	}

	TEST_F(WorkersTests, isMyRace) {
		auto unit = UnitTypeData{};
		setRace(::SC2APIProtocol::Protoss);

		EXPECT_FALSE(isMyRace(unit));

		unit.set_available(true);
		EXPECT_FALSE(isMyRace(unit));

		unit.set_race(NoRace);
		EXPECT_FALSE(isMyRace(unit));

		unit.set_race(race());
		EXPECT_TRUE(isMyRace(unit));
	}

	TEST_F(WorkersTests, isWorkerType) {
		auto unit = UnitTypeData{};

		unit.set_available(true);
		EXPECT_FALSE(isWorkerType(unit));

		unit.set_name("SCV"s);
		EXPECT_TRUE(isWorkerType(unit));

		unit.set_name("Drone"s);
		EXPECT_TRUE(isWorkerType(unit));

		unit.set_name("Probe"s);
		EXPECT_TRUE(isWorkerType(unit));
	}

	auto workerType(Race const r, uint64_t const id) {
		auto ret = UnitTypeData{};

		ret.set_mineral_cost(50);
		ret.set_food_required(1);
		ret.set_unit_id(id);
		ret.set_available(true);
		ret.set_name("Probe"s);
		ret.set_race(r);

		return ret;
	}

	TEST_F(WorkersTests, FoundWorker) {
		auto unit = Unit{};
		auto unit_id = 44ul;
		auto type_id = 43u;
		setRace(::SC2APIProtocol::Protoss);

		onUnitTypeData(workerType(race(), type_id));

		unit.set_tag(unit_id);
		unit.set_owner(id());
		unit.set_unit_type(type_id);

		EXPECT_TRUE(isMyWorker(unit));

		onUnit(unit);
		EXPECT_NE(workers.end(), workers.find(unit_id));

		onDeath(unit_id);
		EXPECT_EQ(workers.end(), workers.find(unit_id));
	}

	TEST_F(WorkersTests, NoIdleWorker) {
		auto a = call();

		EXPECT_FALSE(a.has_value());
	}

	TEST_F(WorkersTests, IdleWorker) {
		{
			auto unit = Unit{};
			auto type_id = 43u;
			setRace(::SC2APIProtocol::Protoss);

			onUnitTypeData(workerType(race(), type_id));

			unit.set_owner(id());
			unit.set_unit_type(type_id);

			unit.set_tag(40u);
			onUnit(unit);

			unit.set_tag(41u);
			onUnit(unit);
		}

		auto a = call();
		auto b = call();
		auto c = call();

		EXPECT_TRUE(a.has_value());
		EXPECT_TRUE(b.has_value());
		EXPECT_FALSE(c.has_value());

		EXPECT_FALSE(call(a).has_value());
		EXPECT_FALSE(call(b).has_value());

		auto d = call();
		EXPECT_TRUE(d.has_value());

		onDeath(40u);
		onDeath(41u);
	}

	TEST_F(WorkersTests, QueueWorkers) {
		setRace(::SC2APIProtocol::Zerg);
		onUnitTypeData(workerType(race(), 42u));

		queueWorkers();

		auto inf = PlayerCommon{};

		inf.set_minerals(50);
		inf.set_food_cap(13);
		inf.set_food_used(12);

		onPlayer(inf);

		EXPECT_CALL(*train, defCall(_))
			.WillOnce(Return(true));

		queueWorkers();
	}
}
