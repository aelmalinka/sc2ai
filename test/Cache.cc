/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Cache.hh"
#include "s2clientprotocol/sc2api.pb.h"

using namespace std;
using namespace testing;
using namespace Sc2Ai;

using ::SC2APIProtocol::Unit;
using ::SC2APIProtocol::PassengerUnit;
using ::SC2APIProtocol::Effect;
using ::SC2APIProtocol::AbilityData;
using ::SC2APIProtocol::UnitTypeData;
using ::SC2APIProtocol::UpgradeData;
using ::SC2APIProtocol::BuffData;
using ::SC2APIProtocol::EffectData;

namespace {
	TEST(CacheTests, BasicStl) {
		auto a = Unit{};
		auto b = Unit{};
		auto c = Unit{};
		auto d = Unit{};

		a.set_tag(1u);
		b.set_tag(2u);
		c.set_tag(3u);
		d.set_tag(4u);

		auto e = Cache{
			a, b, c, d
		};
		auto f = e;
		auto g = Cache<Unit>{};
		auto h = Cache<Unit>();

		EXPECT_EQ(e, f);
		EXPECT_NE(f, g);
		EXPECT_EQ(g, h);

		EXPECT_FALSE(e.empty());
		EXPECT_TRUE(g.empty());

		swap(f, h);

		EXPECT_TRUE(f.empty());
		EXPECT_FALSE(h.empty());

		EXPECT_EQ(e, h);
		EXPECT_NE(e, f);
		EXPECT_EQ(g, f);
		EXPECT_NE(g, h);

		auto i = e.begin();
		auto j = e.cbegin();
		auto k = e.rbegin();
		auto l = e.crbegin();

		auto m = e.end();
		auto n = e.cend();
		auto o = e.rend();
		auto p = e.crend();

		EXPECT_NE(i, m);
		EXPECT_NE(j, n);
		EXPECT_NE(k, o);
		EXPECT_NE(l, p);

		while(i != m) ++i;
		while(j != n) ++j;
		while(k != o) ++k;
		while(l != p) ++l;

		EXPECT_EQ(i, m);
		EXPECT_EQ(j, n);
		EXPECT_EQ(k, o);
		EXPECT_EQ(l, p);

		e.clear();
		h.clear();

		EXPECT_EQ(e, f);
		EXPECT_EQ(e, g);
		EXPECT_EQ(e, h);
	}

	TEST(CacheTests, Access) {
		auto a = AbilityData{};
		auto b = AbilityData{};
		auto c = AbilityData{};
		auto d = AbilityData{};

		a.set_ability_id(1u);
		b.set_ability_id(2u);
		c.set_ability_id(3u);
		d.set_ability_id(4u);

		auto e = Cache<AbilityData>{
			a, b, c, d
		};

		EXPECT_EQ(e[c.ability_id()].ability_id(), c.ability_id());
		EXPECT_EQ(e[b.ability_id()].ability_id(), b.ability_id());

		e[b.ability_id()].set_link_name("asdf"s);

		ASSERT_TRUE(e[b.ability_id()].has_link_name());
		EXPECT_EQ(e[b.ability_id()].link_name(), "asdf"s);
	}

	TEST(CacheTests, Filter) {
		auto g = "The One We Want"s;

		auto a = AbilityData{};
		auto b = AbilityData{};
		auto c = AbilityData{};
		auto d = AbilityData{};

		a.set_ability_id(1u);
		b.set_ability_id(2u);
		c.set_ability_id(3u);
		d.set_ability_id(4u);

		b.set_link_name(g);
		d.set_link_name(g);

		auto e = Cache<AbilityData>{};
		auto f = Cache<AbilityData>{};

		f.filter([&g](auto const &a) -> bool {
			return a.link_name() == g;
		});

		e.insert(a); f.insert(a);
		e.insert(b); f.insert(b);
		e.insert(c); f.insert(c);
		e.insert(d); f.insert(d);

		EXPECT_EQ(e.size(), 4ul);
		EXPECT_EQ(f.size(), 2ul);

		EXPECT_TRUE(e.remove(1u));
		EXPECT_TRUE(e.remove(3u));
		EXPECT_FALSE(f.remove(1u));
		EXPECT_FALSE(f.remove(3u));

		EXPECT_EQ(e, f);
	}

	TEST(CacheDetailGet_id, Unit) {
		Unit a;

		a.set_tag(10u);

		EXPECT_EQ(a.tag(), detail::get_id(a));
	}
	TEST(CacheDetailGet_id, PassengerUnit) {
		PassengerUnit a;

		a.set_tag(10u);

		EXPECT_EQ(a.tag(), detail::get_id(a));
	}
	TEST(CacheDetailGet_id, Effect) {
		Effect a;

		a.set_effect_id(10u);

		EXPECT_EQ(a.effect_id(), detail::get_id(a));
	}
	TEST(CacheDetailGet_id, AbilityData) {
		AbilityData a;

		a.set_ability_id(10u);

		EXPECT_EQ(a.ability_id(), detail::get_id(a));
	}
	TEST(CacheDetailGet_id, UnitTypeData) {
		UnitTypeData a;

		a.set_unit_id(10u);

		EXPECT_EQ(a.unit_id(), detail::get_id(a));
	}
	TEST(CacheDetailGet_id, UpgradeData) {
		UpgradeData a;

		a.set_upgrade_id(10u);

		EXPECT_EQ(a.upgrade_id(), detail::get_id(a));
	}
	TEST(CacheDetailGet_id, BuffData) {
		BuffData a;

		a.set_buff_id(10u);

		EXPECT_EQ(a.buff_id(), detail::get_id(a));
	}
	TEST(CacheDetailGet_id, EffectData) {
		EffectData a;

		a.set_effect_id(10u);

		EXPECT_EQ(a.effect_id(), detail::get_id(a));
	}
}
