/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../src/PlayVsAi.hh"

using namespace std;
using namespace testing;
using namespace Sc2Ai;

using ::SC2APIProtocol::in_game;
using ::SC2APIProtocol::ended;

namespace {
	class MockApi :
		public Sc2Api
	{
		public:
			MockApi(decltype(declval<MockApi>().io()) i, Responder &r) :
				Sc2Api(i, r)
			{}
			MOCK_METHOD(void, send, (Request const &));
	};

	class MockPlayVsAi :
		public PlayVsAi
	{
		public:
			MockPlayVsAi() :
				PlayVsAi("MockPlayVsAi"s, {})
			{}
			void onReady() override {
				PlayVsAi::onReady();
			}
			MOCK_METHOD(void, requestCreateGame, (RequestCreateGame *), (override));
			MOCK_METHOD(void, requestJoinGame, (ResponseCreateGame const &, RequestJoinGame *), (override));
	};

	class PlayVsAiTests :
		public Test
	{
		private:
			net::io_context io;
		protected:
			Request req;
			shared_ptr<StrictMock<MockPlayVsAi>> mod;
			shared_ptr<StrictMock<MockApi>> api;
		public:
			PlayVsAiTests() :
				io{},
				req{},
				mod(make_shared<StrictMock<MockPlayVsAi>>()),
				api(make_shared<StrictMock<MockApi>>(io, *mod))
			{
				mod->setApi(api);
			}
			~PlayVsAiTests() {
				mod->setApi(shared_ptr<Sc2Api>());
			}
	};

	TEST_F(PlayVsAiTests, CreateGame) {
		EXPECT_CALL(*api, send(_))
			.WillOnce(SaveArg<0>(&req));
		EXPECT_CALL(*mod, requestCreateGame(_));

		mod->onReady();

		EXPECT_TRUE(req.has_create_game());
	}

	TEST_F(PlayVsAiTests, JoinGame) {
		EXPECT_CALL(*api, send(_))
			.WillOnce(SaveArg<0>(&req));
		EXPECT_CALL(*mod, requestJoinGame(_, _));

		auto res = Response{};
		res.mutable_create_game();

		mod->onResponse(res);

		EXPECT_TRUE(req.has_join_game());
	}

	TEST_F(PlayVsAiTests, GameInfo) {
		EXPECT_CALL(*api, send(_))
			.WillOnce(SaveArg<0>(&req));

		auto res = Response{};
		res.mutable_join_game();

		mod->onResponse(res);

		EXPECT_TRUE(req.has_game_info());
	}

	TEST_F(PlayVsAiTests, ObservationGameInfo) {
		EXPECT_CALL(*api, send(_))
			.WillOnce(SaveArg<0>(&req));

		auto res = Response{};
		res.mutable_game_info();

		mod->onResponse(res);

		EXPECT_TRUE(req.has_observation());
	}

	TEST_F(PlayVsAiTests, ObservationStep) {
		EXPECT_CALL(*api, send(_))
			.WillOnce(SaveArg<0>(&req));

		auto res = Response{};
		res.mutable_step();

		mod->onResponse(res);

		EXPECT_TRUE(req.has_observation());
	}

	TEST_F(PlayVsAiTests, Step) {
		EXPECT_CALL(*api, send(_))
			.WillOnce(SaveArg<0>(&req));

		auto res = Response{};
		res.set_status(in_game);
		res.mutable_observation();

		mod->onResponse(res);

		EXPECT_TRUE(req.has_step());
	}

	TEST_F(PlayVsAiTests, Quit) {
		EXPECT_CALL(*api, send(_))
			.WillOnce(SaveArg<0>(&req));

		auto res = Response{};
		res.set_status(ended);
		res.mutable_observation();

		mod->onResponse(res);

		EXPECT_TRUE(req.has_quit());
	}
}
