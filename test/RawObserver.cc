/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../src/RawObserver.hh"

using namespace std;
using namespace testing;
using namespace Sc2Ai;

using ::SC2APIProtocol::Zerg;

namespace {
	class MockObserver :
		public RawObserver
	{
		public:
			MockObserver() :
				RawObserver("MockRawObserver", {})
			{}
			MOCK_METHOD(void, onStart, (StartRaw const &), (override));
			MOCK_METHOD(void, onPlayerMock, (PlayerRaw const &), ());
			MOCK_METHOD(void, onPowerSource, (PowerSource const &), (override));
			MOCK_METHOD(void, onUpgrade, (uint32_t const &), (override));
			MOCK_METHOD(void, onUnit, (Unit const &), (override));
			MOCK_METHOD(void, onDeath, (uint64_t const &), (override));
			MOCK_METHOD(void, onEffect, (Effect const &), (override));
			void onPlayer(PlayerRaw const &raw) override {
				RawObserver::onPlayer(raw);
				onPlayerMock(raw);
			}
	};

	TEST(RawObserverTests, GameInfoNoRace) {
		StrictMock<MockObserver> mock;

		auto res = Response{};
		res.mutable_game_info();

		EXPECT_THROW(mock.onResponse(res), Exception);
	}

	TEST(RawObserverTests, NotRawGameInfo) {
		StrictMock<MockObserver> mock;
		auto id = 52u;

		auto join = Response{};
		join.mutable_join_game()->set_player_id(id);

		auto res = Response{};
		auto inf = res.mutable_game_info();
		auto player = inf->add_player_info();

		player->set_player_id(id);
		player->set_race_actual(Zerg);

		mock.onResponse(join);
		EXPECT_THROW(mock.onResponse(res), Exception);
	}

	TEST(RawObserverTests, NotRawObservation) {
		StrictMock<MockObserver> mock;

		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		res.mutable_observation()->mutable_observation();

		mock.onResponse(start);
		EXPECT_THROW(mock.onResponse(res), Exception);
	}

	TEST(RawObserverTests, onStart) {
		StrictMock<MockObserver> mock;
		auto id = 52u;

		auto join = Response{};
		join.mutable_join_game()->set_player_id(id);

		auto res = Response{};
		auto inf = res.mutable_game_info();
		auto player = inf->add_player_info();

		inf->mutable_start_raw();

		player->set_player_id(id);
		player->set_race_actual(Zerg);

		EXPECT_CALL(mock, onStart(_));

		mock.onResponse(join);
		mock.onResponse(res);
	}

	TEST(RawObserverTests, onPlayer) {
		StrictMock<MockObserver> mock;

		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		res.mutable_observation()->mutable_observation()->mutable_raw_data()->mutable_player();

		EXPECT_CALL(mock, onPlayerMock(_));

		mock.onResponse(start);
		mock.onResponse(res);
	}

	TEST(RawObserverTests, onPowerSource) {
		StrictMock<MockObserver> mock;
		
		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		auto raw = res.mutable_observation()->mutable_observation()->mutable_raw_data()->mutable_player();

		raw->add_power_sources();
		raw->add_power_sources();

		EXPECT_CALL(mock, onPlayerMock(_));
		EXPECT_CALL(mock, onPowerSource(_))
			.Times(2);

		mock.onResponse(start);
		mock.onResponse(res);
	}

	TEST(RawObserverTests, onUpgrade) {
		StrictMock<MockObserver> mock;

		auto id1 =42u;
		auto id2 =43u;
		
		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		auto raw = res.mutable_observation()->mutable_observation()->mutable_raw_data()->mutable_player();

		raw->add_upgrade_ids(id1);
		raw->add_upgrade_ids(id2);

		EXPECT_CALL(mock, onPlayerMock(_));
		EXPECT_CALL(mock, onUpgrade(id1));
		EXPECT_CALL(mock, onUpgrade(id2));

		mock.onResponse(start);
		mock.onResponse(res);
	}

	TEST(RawObserverTests, onUnit) {
		StrictMock<MockObserver> mock;

		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		auto raw = res.mutable_observation()->mutable_observation()->mutable_raw_data();

		raw->add_units();
		raw->add_units();

		EXPECT_CALL(mock, onUnit(_))
			.Times(2);

		mock.onResponse(start);
		mock.onResponse(res);
	}

	TEST(RawObserverTests, onDeath) {
		StrictMock<MockObserver> mock;

		auto id1 =42u;
		auto id2 =43u;
		
		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		auto raw = res.mutable_observation()->mutable_observation()->mutable_raw_data()->mutable_event();

		raw->add_dead_units(id1);
		raw->add_dead_units(id2);

		EXPECT_CALL(mock, onDeath(id1));
		EXPECT_CALL(mock, onDeath(id2));

		mock.onResponse(start);
		mock.onResponse(res);
	}

	TEST(RawObserverTests, onEffect) {
		StrictMock<MockObserver> mock;

		auto start = Response{}; start.mutable_data();
		auto res = Response{};
		auto raw = res.mutable_observation()->mutable_observation()->mutable_raw_data();

		raw->add_effects();
		raw->add_effects();

		EXPECT_CALL(mock, onEffect(_))
			.Times(2);

		mock.onResponse(start);
		mock.onResponse(res);
	}
}
