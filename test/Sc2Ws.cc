/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Sc2Ws.hh"

using namespace std;
using namespace testing;
using namespace Sc2Ai;

namespace {
	class TestSc2Ws :
		public Sc2Ws
	{
		public:
			TestSc2Ws(
				decltype(declval<TestSc2Ws>().io()) i,
				string const &u
			) :
				Sc2Ws(i, u)
			{}
		protected:
			void onResponse(Response &&) override {}
	};

	class Sc2WsTests :
		public Test
	{
		protected:
			net::io_context io;
			shared_ptr<Sc2Ws> ws;
	};

	TEST_F(Sc2WsTests, Create) {
		ws = make_shared<TestSc2Ws>(io, "http://localhost/"s);
	}
}
