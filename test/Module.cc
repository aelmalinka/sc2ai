/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../src/Module.hh"

using namespace std;
using namespace testing;
using namespace Sc2Ai;

namespace {
	class MockModuleDep :
		public Module
	{
		public:
			MockModuleDep() :
				Module("MockModuleDep"s, {})
			{}
			MOCK_METHOD(void, onLoaded, (), (override));
	};

	class MockModule :
		public Module
	{
		public:
			MockModule() :
				Module("MockModule"s, { "MockModuleDep"s })
			{
				add<int>(bind(&MockModule::IntCall, this, placeholders::_1));
				add<string>(bind(&MockModule::StringCall, this, placeholders::_1));
			}
			MOCK_METHOD(void, onLoaded, (), (override));
			MOCK_METHOD(void, IntCall, (int), ());
			MOCK_METHOD(int, StringCall, (string const &), ());
			auto dep(string const &name) -> decltype(Module::dep(""s)) & {
				return Module::dep(name);
			}
			auto app() -> decltype(Module::app()) const & {
				return Module::app();
			}
			auto api() -> decltype(Module::api()) const & {
				return Module::api();
			}
	};

	char const *args[] = {
		"ModuleTests",
	};

	class MockApplication :
		public Application
	{
		private:
			std::map<std::string, Coeus::Import<Module>> _mods;
		public:
			MockApplication(shared_ptr<MockModuleDep> const &dep) :
				Application(1, args),
				_mods{}
			{
				_mods["MockModuleDep"s] = dep;
			}
		protected:
			decltype(_mods) &modules() override {
				return _mods;
			}
	};

	class MockResponder :
		public Responder
	{
		public:
			MOCK_METHOD(void, onError, (decltype(declval<Response>().error()) const &), (override));
	};

	class ModuleTests :
		public Test
	{
		protected:
			net::io_context io;
			StrictMock<MockResponder> res;
			std::shared_ptr<StrictMock<MockModuleDep>> dep;
			std::shared_ptr<MockApplication> app;
			std::shared_ptr<Sc2Api> api;
			std::shared_ptr<StrictMock<MockModule>> mod;
		public:
			ModuleTests() :
				io(),
				dep(make_shared<StrictMock<MockModuleDep>>()),
				app(make_shared<MockApplication>(dep)),
				api(make_shared<Sc2Api>(io, res, "http://localhost/"s)),
				mod(make_shared<StrictMock<MockModule>>())
			{
				dep->setApplication(app);
				mod->setApplication(app);
				mod->setApi(api);
			}
	};

	TEST_F(ModuleTests, DepExists) {
		EXPECT_EQ(dep.get(), &*mod->dep("MockModuleDep"s));
	}

	TEST_F(ModuleTests, AppSet) {
		EXPECT_EQ(&mod->app(), app.get());
	}

	TEST_F(ModuleTests, ApiSet) {
		EXPECT_EQ(&mod->api(), api.get());
	}

	TEST_F(ModuleTests, CallInt) {
		auto v = 42;
		EXPECT_CALL(*mod, IntCall(v));
		EXPECT_FALSE(mod->call(v).has_value());
	}

	TEST_F(ModuleTests, CallString) {
		auto v = 67;
		auto s = "Hello Module!"s;
		EXPECT_CALL(*mod, StringCall(s))
			.WillOnce(Return(v));

		auto r = mod->call(s);

		ASSERT_TRUE(r.has_value());
		ASSERT_NO_THROW(any_cast<int>(r));
		EXPECT_EQ(any_cast<int>(r), v);
	}
}
