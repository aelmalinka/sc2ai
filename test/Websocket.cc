/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Websocket.hh"

using namespace std;
using namespace testing;
using namespace Sc2Ai;

namespace {
	class TestWs :
		public Websocket
	{
		public:
			TestWs(net::io_context &io, string const &w) :
				Websocket(io, w)
			{}
		protected:
			void onMessage(string &&) override {
			}
	};

	class WebsocketTests :
		public Test
	{
		protected:
			net::io_context io;
			std::shared_ptr<TestWs> ws;
	};

	// 2020-04-18 AMR TODO: server to connect to
	TEST_F(WebsocketTests, Create) {
		ws = make_shared<TestWs>(io, "http://localhost/"s);
	}
}
