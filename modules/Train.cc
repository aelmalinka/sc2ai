/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "../src/RawObserver.hh"
#include "../src/Cache.hh"

using namespace std;
using namespace Sc2Ai;

using ::SC2APIProtocol::Zerg;

namespace {
	// 2020-05-28 AMR TODO: support not zerg
	// 2020-05-31 AMR TODO: return new units id? not likely
	// 2020-07-30 AMR TODO: detect supply bumps and pre-build?
	class Train :
		public RawObserver
	{
		private:
			// 2020-05-31 AMR TODO: share these?
			Cache<UnitTypeData> units;
			Cache<UnitTypeData> training_grounds_types;
			Cache<Unit> training_grounds;
		public:
			Train();
			void onLoaded() override;
			void onUnitTypeData(UnitTypeData const &) override;
			void onUnit(Unit const &) override;
			void onDeath(uint64_t const &) override;
		protected:
			auto trainUnit(uint32_t const) -> bool;
		protected:
			auto isTrainingGroundType(UnitTypeData const &) const -> bool;
			auto isTrainingGround(Unit const &) const -> bool;
	};

	Train::Train() :
		RawObserver("Train"s, {"Supply"s}),
		training_grounds_types(bind(&Train::isTrainingGroundType, this, placeholders::_1)),
		training_grounds(bind(&Train::isTrainingGround, this, placeholders::_1))
	{
		add<uint32_t>(bind(&Train::trainUnit, this, placeholders::_1));
	}

	void Train::onLoaded() {
		app().data()->UnitTypes();
	}

	void Train::onUnitTypeData(UnitTypeData const &data) {
		units.insert(data);

		if(training_grounds_types.insert(data))
			COEUS_LOG(Log, Severity::Debug) << "Found Training ground type " << data.unit_id();
	}

	void Train::onUnit(Unit const &data) {
		if(training_grounds.insert(data))
			COEUS_LOG(Log, Severity::Debug) << "Found Training ground " << data.tag();
	}

	void Train::onDeath(uint64_t const &data) {
		if(training_grounds.remove(data))
			COEUS_LOG(Log, Severity::Debug) << "Lost Training ground " << data;
	}

	auto Train::trainUnit(uint32_t const id) -> bool {
		auto acts = vector<Action>{};
		auto com = acts.emplace_back().mutable_action_raw()->mutable_unit_command();

		auto &unit = units[id];

		// 2020-05-28 AMR TODO: queue multiple buildings (not zerg)
		// 2020-05-29 AMR TODO: don't queue if full?
		// 2020-06-01 AMR TODO: check for resources?
		// 2020-07-30 AMR TODO: queen
		if(race() == Zerg && !training_grounds.empty()) {
			auto i = training_grounds.begin();

			com->set_ability_id(unit.ability_id());
			com->add_unit_tags(i->tag());

			COEUS_LOG(Log, Severity::Debug) << "Training unit " << unit.name() << " at " << i->tag();
			doAction(move(acts));

			auto id = i->tag();
			training_grounds.remove(id);

			return true;
		}

		//COEUS_LOG(Log, Severity::Debug) << "No training ground found";
		return false;
	}

	auto Train::isTrainingGroundType(UnitTypeData const &data) const -> bool {
		return data.name() == "Larva"s;
	}

	auto Train::isTrainingGround(Unit const &data) const -> bool {
		if(training_grounds_types.empty())
			COEUS_THROW(Exception("Checking for training ground w/o type"));

		if(data.has_unit_type())
			for(auto &t : training_grounds_types)
				if(t.unit_id() == data.unit_type())
					return true;

		return false;
	}

	COEUS_MODULE(Train)
}
