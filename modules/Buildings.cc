/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "../src/RawObserver.hh"
#include "../src/Cache.hh"
#include "../src/PNG.hh"

using namespace std;
using namespace Sc2Ai;

using ::SC2APIProtocol::Structure;
using ::SC2APIProtocol::ImageData;
using ::SC2APIProtocol::Terran;
using ::SC2APIProtocol::Zerg;
using ::SC2APIProtocol::Protoss;
using ::SC2APIProtocol::Race_Name;

#	define GET_OR_LOG(obj, field, res) \
		if(!obj.has_ ## field()) COEUS_LOG(Log, Severity::Error) << "failed to get field " # field; \
		else res = obj.field();

namespace {
#	ifdef DEBUG
		static uint8_t get_image_data1(ImageData const &data, uint32_t const x, uint32_t const y) {
			auto off = (data.size().x() * y + x) / 8;
			auto const &byte = data.data()[off];
			return byte & (1 << x % 8) ? 0xFF : 0x00;
		}

		static uint8_t get_image_data8(ImageData const &data, uint32_t const x, uint32_t const y) {
			auto off = data.size().x() * y + x;
			return data.data()[off];
		}

		static uint8_t get_image_data(ImageData const &data, uint32_t const x, uint32_t const y) {
			if(data.bits_per_pixel() == 8)
				return get_image_data8(data, x, y);
			else if(data.bits_per_pixel() == 1)
				return get_image_data1(data, x, y);
			else
				COEUS_THROW(Exception("Unsupported bits per pixel"s));
		}

		static void write_image_data(string const &name, ImageData const &data) {
			PNG image(data.size().x(), data.size().y());
			for(auto x = 0u, y = 0u; x < data.size().x() && y < data.size().y();) {

				image.pixel(x, y) = get_image_data(data, x, y);

				if(++x == data.size().x()) {
					++y; x = 0;
				}
			}

			image.write(name);
		}
#	endif

	// 2020-07-30 AMR TODO: creep when zerg; power field when protoss; add-ons when terran
	class Buildings :
		public RawObserver
	{
		public:
			using Id = uint64_t;
			using Position = tuple<float, float>;
			using Callback = function<void(Id const)>;
			using BuildParameterType = tuple<Id, Position, Position, Callback>;
		private:
			uint32_t minerals;
			uint32_t gas;
			Cache<UnitTypeData> building_types;
			Cache<AbilityData> building_abilities;
			Cache<Unit> workers;
			Cache<Unit> buildings;
			Cache<Unit> buildings_in_progress;
			map<Id, Callback> callbacks;
			map<Id, Id> worker_buildings;
			list<Id> worker_ids;
			ImageData placement_grid;
			// 2020-06-08 AMR TODO: pathing grid? (check for blocks?)
		public:
			Buildings();
		protected:
			void onLoaded() override;
			void onStart(StartRaw const &) override;
			void onUnitTypeData(UnitTypeData const &) override;
			void onAbilityData(AbilityData const &) override;
			void onPlayer(PlayerCommon const &) override;
			void onUnit(Unit const &) override;
			void onDeath(uint64_t const &) override;
			auto build(Id const, Position const &, Position const &, Callback const &) -> bool;
			auto finish(Unit const &) -> void;
			auto inProgress(Unit const &) -> void;
			auto doBuild(Id const, Id const, Position const &) -> void;
			auto fits(Id const, Position const &) -> bool;
			auto isBuildable(float const, float const) -> bool;
			auto saveCb(Id const &, Callback const &) -> void;
			auto getBuildAbility(Id const) const -> AbilityData const &;
			auto getWorker() -> Id;
			auto forgetWorker(Id const) -> void;
			auto returnWorker(Id const) -> void;
			auto setBuilt(Unit const &) -> void;
			auto setBuildable(Unit const &) -> void;
			auto setBit(float const, float const, bool const) -> void;
		protected:
			auto findWorker(Unit const &) -> decltype(workers.begin());
			auto isMyWorker(Unit const &) const -> bool;
			auto isMyBuilding(Unit const &) -> bool;
			auto isBuilding(Unit const &) const -> bool;
			auto isBuildBuilding(AbilityData const &) const -> bool;
			auto isStructure(UnitTypeData const &) const -> bool;
			template<typename T>
			auto isAvailable(T const &) const -> bool;
	};

	Buildings::Buildings() :
		RawObserver("Buildings"s, {"Workers"s}),
		minerals(0u),
		gas(0u),
		building_types(bind(&Buildings::isStructure, this, placeholders::_1)),
		building_abilities(bind(&Buildings::isBuildBuilding, this, placeholders::_1)),
		workers(bind(&Buildings::isMyWorker, this, placeholders::_1)),
		buildings(bind(&Buildings::isBuilding, this, placeholders::_1)),
		worker_ids()
	{
		add<Id, Position, Position, Callback>(bind(&Buildings::build, this, placeholders::_1, placeholders::_2, placeholders::_3, placeholders::_4));
		add<Id, Position>(bind(&Buildings::fits, this, placeholders::_1, placeholders::_2));
		// 2020-07-30 AMR TODO: unit id's are 64-bit but type ids are 32bit; let's just take both?
		add<uint32_t, Position, Position, Callback>(bind(&Buildings::build, this, placeholders::_1, placeholders::_2, placeholders::_3, placeholders::_4));
		add<uint32_t, Position>(bind(&Buildings::fits, this, placeholders::_1, placeholders::_2));
	}

	void Buildings::onLoaded() {
		app().data()->UnitTypes();
		app().data()->Abilities();
	}

	void Buildings::onStart(StartRaw const &start) {
		COEUS_LOG(Log, Severity::Info) << "Playing on " << start.map_size().x() << "x" << start.map_size().y();
		placement_grid = start.placement_grid();
#		ifdef DEBUG
			write_image_data("replays/original-placement.png"s, placement_grid);
			write_image_data("replays/terrain-height.png"s, start.terrain_height());
			write_image_data("replays/pathing-grid.png"s, start.pathing_grid());
#		endif
	}

	void Buildings::onUnitTypeData(UnitTypeData const &unit) {
		building_types.insert(unit);
	}

	void Buildings::onAbilityData(AbilityData const &ability) {
		building_abilities.insert(ability);
	}

	void Buildings::onPlayer(PlayerCommon const &player) {
		GET_OR_LOG(player, minerals, minerals);
		GET_OR_LOG(player, vespene, gas);
	}

	void Buildings::onUnit(Unit const &unit) {
		if(workers.insert(unit))
			COEUS_LOG(Log, Severity::Info) << "found worker building " << unit.tag();
		if(buildings.insert(unit)) {
			setBuilt(unit);
			inProgress(unit);
		}

		if(isMyBuilding(unit) && unit.build_progress() >= 1.0f)
			finish(unit);
	}

	void Buildings::onDeath(uint64_t const &unit) {
		if(auto i = buildings.find(unit); i != buildings.end()) {
			setBuildable(*i);

			// 2020-07-23 AMR TODO: notify dead in progress; or build new or what?
			if(buildings.remove(unit))
				COEUS_LOG(Log, Severity::Info) << "building " << unit << " died";
		}

		// 2020-06-10 AMR TODO: assign new worker; or notify or something
		if(workers.remove(unit))
			COEUS_LOG(Log, Severity::Error) << "worker building something died " << unit;
		if(buildings_in_progress.remove(unit))
			COEUS_LOG(Log, Severity::Error) << "Building was in progress when it died " << unit;
	}

	auto Buildings::build(Id const id, Position const &start, Position const &to, Callback const &cb) -> bool {
		COEUS_LOG(Log, Severity::Debug) << "attempting to build " << id << " from "
			<< get<0>(start) << " " << get<1>(start) << " to " << get<0>(to) << " " << get<1>(to);
		auto loc = start;
		auto left_last = false;
		auto const &building = building_types[id];

		auto dirx = (get<0>(to) > get<0>(loc) ? 1.0f : -1.0f);
		auto diry = (get<1>(to) > get<1>(loc) ? 1.0f : -1.0f);

		if(
			minerals < building.mineral_cost() ||
			gas < building.vespene_cost()
		) {
			COEUS_LOG(Log, Severity::Info) << "building costs not met; not building; minerals: " << building.mineral_cost() << " vs " << minerals << " gas: " << building.vespene_cost() << " vs " << gas;
			return false;
		}
		while(
			(dirx > 0 ? get<0>(loc) <= get<0>(to) : get<0>(to) <= get<0>(loc)) &&
			(diry > 0 ? get<1>(loc) <= get<1>(to) : get<1>(to) <= get<1>(loc))
		) {
			if(fits(id, loc)) {
				auto i = getWorker();

				if(i == -1u) {
					COEUS_LOG(Log, Severity::Error) << "found location but didn't get a worker";
					return false;
				}

				doBuild(id, i, loc);
				saveCb(i, cb);
				return true;
			}

			if(!left_last)
				get<0>(loc) += 1.0f * dirx;
			else
				get<1>(loc) += 1.0f * diry;

			left_last = !left_last;
		}

		COEUS_LOG(Log, Severity::Debug) << "doesn't fit between " << get<0>(start) << "," << get<1>(start) << " and " << get<0>(to) << ", " << get<1>(to);
		return false;
	}

	auto Buildings::finish(Unit const &building) -> void {
		COEUS_LOG(Log, Severity::Debug) << "building finished " << building.tag();
		auto id = building.tag();
		auto f = callbacks[id];

		if(f) f(id);
		callbacks.erase(id);

		auto worker = worker_buildings.find(id);
		if(worker != worker_buildings.end()) {
			returnWorker(worker->second);
		}

		buildings_in_progress.remove(id);
	}

	auto Buildings::inProgress(Unit const &building) -> void {
		auto worker = findWorker(building);
		if(worker == workers.end()) {
			return;
		}

		if(buildings_in_progress.insert(building))
		{
			COEUS_LOG(Log, Severity::Debug) << "changing callback key from " << worker->tag() << " to " << building.tag();
			auto n = callbacks.extract(worker->tag());
			n.key() = building.tag();
			callbacks.insert(move(n));
		}

		if(race() == Zerg) {
			forgetWorker(worker->tag());
		} else if(race() == Protoss) {
			returnWorker(worker->tag());
		} else if(race() == Terran) {
			worker_buildings[building.tag()] = worker->tag();
		} else {
			COEUS_LOG(Log, Severity::Error) << "unknwon race " << Race_Name(race());
		}
	}

	// 2020-06-10 AMR TODO: this is truncate; do we want ceil?
	auto Buildings::isBuildable(float const x, float const y) -> bool {
		if(placement_grid.bits_per_pixel() != 1)
			COEUS_THROW(Exception("Blizzard changed bits_per_pixel on placement_grid"));

		size_t off = (placement_grid.size().x() * static_cast<size_t>(y) + static_cast<size_t>(x)) * placement_grid.bits_per_pixel() / 8;
		size_t sz = placement_grid.size().x() * placement_grid.size().y() * placement_grid.bits_per_pixel() / 8;
		// 2020-06-10 AMR NOTE: can't build off the edge of the map
		if(off > 0 && off < sz) {
			auto const &byte = placement_grid.data()[off];
			// 2020-06-10 AMR TODO: bits per pixel may not be 1 (very unlikely)
			if(byte & (1 << static_cast<size_t>(x) % 8)) {
				return true;
			}
		}

		return false;
	}

	auto Buildings::fits(Id const id, Position const &where) -> bool {
		auto const &ability = getBuildAbility(id);
		auto left = floor(get<0>(where)) - ability.footprint_radius();
		auto right = floor(get<0>(where)) + ability.footprint_radius();
		auto bottom = floor(get<1>(where)) - ability.footprint_radius();
		auto top = floor(get<1>(where)) + ability.footprint_radius();

		for(auto i = left; i <= right; i++) {
			for(auto j = bottom; j <= top; j++) {
				if(!isBuildable(i, j)) {
					return false;
				}
			}
		}

		return true;
	}

	auto Buildings::doBuild(Id const id, Id const worker, Position const &where) -> void {
		auto acts = vector<Action>{};
		auto com1 = acts.emplace_back().mutable_action_raw()->mutable_unit_command();
		auto const &ability = getBuildAbility(id);

		COEUS_LOG(Log, Severity::Info) << "building " << id << " with worker " << worker << " at " << get<0>(where) << "x" << get<1>(where);
		com1->set_ability_id(ability.ability_id());
		com1->add_unit_tags(worker);

		auto point = com1->mutable_target_world_space_pos();

		point->set_x(get<0>(where));
		point->set_y(get<1>(where));

		doAction(move(acts));
	}

	auto Buildings::saveCb(Id const &worker, Callback const &f) -> void {
		COEUS_LOG(Log, Severity::Debug) << "saving cb for worker " << worker;
		callbacks.emplace(worker, f);
	}

	auto Buildings::getBuildAbility(Id const id) const -> AbilityData const & {
		auto j = find_if(building_types.begin(), building_types.end(), [&id](auto const &u) -> bool {
			return id == u.unit_id();
		});
		if(j == building_types.end())
			COEUS_THROW(Exception("Attempting to build a building w/o unit data"s));

		auto i = find_if(building_abilities.begin(), building_abilities.end(), [&j](auto const &e) -> bool {
			return j->ability_id() == e.ability_id();
		});

		if(i == building_abilities.end())
			COEUS_THROW(Exception("Attempting to build a building w/o abilitiy"s));

		return *i;
	}

	auto Buildings::getWorker() -> Id {
		try {
			auto ret = dep("Workers"s)->call();

			if(ret.has_value()) {
				auto id = any_cast<Id>(ret);
				COEUS_LOG(Log, Severity::Debug) << "got worker " << id;
				worker_ids.push_back(id);
				return id;
			}
		} catch(bad_any_cast &) {
			COEUS_LOG(Log, Severity::Error) << "unknown worker type returned";
		}

		COEUS_LOG(Log, Severity::Error) << "getWorker failed; returning " << -1u << " this should only happen on bad_any_cast";
		return -1u;
	}

	auto Buildings::forgetWorker(Id const id) -> void {
		COEUS_LOG(Log, Severity::Debug) << "forgetting worker " << id;
		auto i = find(worker_ids.begin(), worker_ids.end(), id);
		if(!workers.remove(id))
			COEUS_LOG(Log, Severity::Error) << "Returning worker that isn't mine " << id;
		if(i == worker_ids.end())
			COEUS_LOG(Log, Severity::Error) << "Returning worker (id) that isn't mine " << id;
		else
			worker_ids.erase(i);
	}

	auto Buildings::returnWorker(Id const id) -> void {
		forgetWorker(id);
		dep("Workers"s)->call(id);
	}

	auto Buildings::setBuilt(Unit const &unit) -> void {
		// 2020-06-16 AMR TODO: z?
		// 2020-06-16 AMR TODO: floating buildings
		COEUS_LOG(Log, Severity::Debug) << "setting " << unit.tag() << " built; type "
			<< unit.unit_type() << " at " << unit.pos().x() << " " << unit.pos().y() << " of size " << unit.radius();
		auto left = unit.pos().x() - unit.radius();
		auto right = unit.pos().x() + unit.radius();
		auto bottom = unit.pos().y() - unit.radius();
		auto top = unit.pos().y() + unit.radius();

		for(auto i = left; i <= right; i++) {
			for(auto j = bottom; j <= top; j++) {
				setBit(j, i, false);
			}
		}
	}

	auto Buildings::setBuildable(Unit const &unit) -> void {
		// 2020-06-16 AMR TODO: z?
		// 2020-06-16 AMR TODO: floating buildings
		COEUS_LOG(Log, Severity::Debug) << "setting " << unit.tag() << " not built; type "
			<< unit.unit_type() << " at " << unit.pos().x() << " " << unit.pos().y() << " of size " << unit.radius();
		auto left = unit.pos().x() - unit.radius();
		auto right = unit.pos().x() + unit.radius();
		auto bottom = unit.pos().y() - unit.radius();
		auto top = unit.pos().y() + unit.radius();

		for(auto i = left; i <= right; i++) {
			for(auto j = bottom; j <= top; j++) {
				setBit(j, i, true);
			}
		}
	}

	auto Buildings::setBit(float const x, float const y, bool const on) -> void {
		if(placement_grid.bits_per_pixel() != 1)
			COEUS_THROW(Exception("Blizzard changed bits_per_pixel on placement_grid"));

		size_t off = (placement_grid.size().x() * static_cast<size_t>(y) + static_cast<size_t>(x)) * placement_grid.bits_per_pixel() / 8;
		size_t sz = placement_grid.size().x() * placement_grid.size().y() * placement_grid.bits_per_pixel() / 8;
		// 2020-06-10 AMR NOTE: can't build off the edge of the map
		if(off > 0 && off < sz) {
			auto &bytes = (*placement_grid.mutable_data())[off];
			if(on)
				bytes |= (1 << static_cast<size_t>(x) % 8);
			else
				bytes &= ~(1 << static_cast<size_t>(x) % 8);
		}
	}

	auto Buildings::findWorker(Unit const &data) -> decltype(workers.begin()) {
		return find_if(workers.begin(), workers.end(), [this, &data](auto const &worker) -> bool {
			return find_if(worker.orders().begin(), worker.orders().end(), [this, &data](auto const &order) -> bool {
				return
					order.ability_id() == building_abilities[data.unit_type()].ability_id() &&
					order.target_unit_tag() == data.tag()
				;
			}) != worker.orders().end();
		});
	}

	auto Buildings::isMyBuilding(Unit const &data) -> bool {
		return
			building_types.find(data.unit_type()) != building_types.end() &&
			(
				buildings_in_progress.find(data.tag()) != buildings_in_progress.end() ||
				findWorker(data) != workers.end()
			)
		;
	}

	auto Buildings::isMyWorker(Unit const &data) const -> bool {
		return find_if(worker_ids.begin(), worker_ids.end(), [&data](auto const &i) -> bool {
			return data.tag() == i;
		}) != worker_ids.end();
	}

	auto Buildings::isBuilding(Unit const &data) const -> bool {
		return building_types.find(data.unit_type()) != building_types.end();
	}

	auto Buildings::isBuildBuilding(AbilityData const &data) const -> bool {
		return isAvailable(data) && data.has_is_building() && data.is_building();
	}

	auto Buildings::isStructure(UnitTypeData const &data) const -> bool {
		if(isAvailable(data))
			for(auto &i : data.attributes())
				if(i == Structure)
					return true;

		return false;
	}

	template<typename T>
	auto Buildings::isAvailable(T const &data) const -> bool {
		return data.has_available() && data.available();
	}

	COEUS_MODULE(Buildings)
}
