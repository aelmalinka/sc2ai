# Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
# Distributed under the terms of the GNU Affero General Public License v3

macro(add_module target cxxfile)
	add_library(${target} MODULE ${cxxfile})
	target_link_libraries(${target} PUBLIC Sc2Ai)
endmacro(add_module)

add_module(Bases Bases.cc)
add_module(Buildings Buildings.cc)
add_module(Supply Supply.cc)
add_module(Train Train.cc)
add_module(VsAi VsAi.cc)
add_module(Workers Workers.cc)
add_module(ZergMacro ZergMacro.cc)
