/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "../src/PlayVsAi.hh"

#include <fstream>
#include <chrono>
#include <boost/lexical_cast.hpp>

using namespace std;
using namespace std::chrono;
using namespace std::filesystem;
using namespace Sc2Ai;

using ::boost::lexical_cast;
using ::SC2APIProtocol::Participant;
using ::SC2APIProtocol::Computer;

namespace {
	class VsAi :
		public PlayVsAi
	{
		private:
			bool _replay_requested;
		protected:
			// 2020-05-07 AMR TODO: config file or app params?
			static constexpr auto replays = "replays";
			static constexpr auto mapname = "DiscoBloodbathLE";
			static constexpr auto mappath = "Ladder2019Season3";
			static constexpr auto my_name = "malinka-bot";
			static constexpr auto op_name = "computer";
			static constexpr auto my_race = ::SC2APIProtocol::Zerg;
			static constexpr auto op_race = ::SC2APIProtocol::Random;
			static constexpr auto op_diff = ::SC2APIProtocol::VeryHard;	// 2020-05-07 AMR NOTE: quick games for now
			static constexpr auto aibuild = ::SC2APIProtocol::Rush;		// 2020-05-07 AMR NOTE: quick games for now
		public:
			VsAi();
		protected:
			void onDone() override;
			void onStateChange(Status const &) override;
			void onSaveReplay(ResponseSaveReplay const &) override;
			void onActionError(ActionError const &) override;
			void requestCreateGame(RequestCreateGame *) override;
			void requestJoinGame(ResponseCreateGame const &, RequestJoinGame *) override;
			void saveReplay(path const &, string const &);
	};

	VsAi::VsAi() :
		PlayVsAi("VsAi"s, {}),
		_replay_requested(false)
	{}

	void VsAi::onStateChange(Status const &state) {
		COEUS_LOG(Log, Severity::Info) << "state changed to " << Status_Name(state);
	}

	void VsAi::onDone() {
		if(!_replay_requested) {
			COEUS_LOG(Log, Severity::Info) << "Game done; requesting replay";

			auto req = Request{};
			req.mutable_save_replay();

			api().send(move(req));

			_replay_requested = true;
		} else {
			COEUS_LOG(Log, Severity::Error) << "onDone called multiple times; why?";
		}
	}

	void VsAi::onSaveReplay(ResponseSaveReplay const &res) {
		COEUS_LOG(Log, Severity::Info) << "Received replay";

		if(!res.has_data()) {
			COEUS_LOG(Log, Severity::Error) << "ResponseSaveReplay without replay data";
		} else if(!exists(replays) && !create_directory(replays)) {
			COEUS_LOG(Log, Severity::Error) << replays << " directory doesn't exist and failed to create";
		} else {
			auto tp = system_clock::now();
			auto fn =
				(path(replays) / mapname).string() + "-"s +
				lexical_cast<string>(duration_cast<seconds>(tp.time_since_epoch()).count()) +
				".SC2Replay"s
			;
			saveReplay(fn, res.data());
		}

		auto req = Request{};
		req.mutable_quit();

		api().send(move(req));
	}

	void VsAi::onActionError(ActionError const &err) {
		stringstream str;
		str << " for unit " << err.unit_tag();

		COEUS_LOG(Log, Severity::Error) << "action failed: " << err.ability_id() << " " << ActionResult_Name(err.result())
			<< (err.has_unit_tag() ? str.str() : ""s);
	}

	void VsAi::saveReplay(path const &fn, string const &data) {
		auto fh = ofstream(fn.string(), ios_base::binary);
		if(!fh) {
			COEUS_LOG(Log, Severity::Error) << "Failed to open";
		} else {
			fh << data;
		}
	}

	void VsAi::requestCreateGame(RequestCreateGame *game) {
		auto map = game->mutable_local_map();
		auto player = game->add_player_setup();
		auto gameai = game->add_player_setup();

		map->set_map_path((path(mappath) / mapname).string() + ".SC2Map"s);

		player->set_type(Participant);
		player->set_player_name(my_name);
		player->set_race(my_race);

		gameai->set_type(Computer);
		gameai->set_difficulty(op_diff);
		gameai->set_race(op_race);
		gameai->set_ai_build(aibuild);
	}

	void VsAi::requestJoinGame(ResponseCreateGame const &, RequestJoinGame *join) {
		join->set_race(my_race);

		auto opts = join->mutable_options();

		opts->set_raw(true);
		opts->set_score(true);
		opts->set_raw_affects_selection(false);
	}

	COEUS_MODULE(VsAi)
}
