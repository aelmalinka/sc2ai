/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "../src/RawObserver.hh"
#include "../src/Cache.hh"
#include <numeric>
#include <cmath>

using namespace std;
using namespace Sc2Ai;

using ::SC2APIProtocol::Structure;
using ::SC2APIProtocol::Point2D;

namespace {
	class Bases :
		public RawObserver
	{
		private:
			static constexpr auto base_safe_dist = 6.0f;
			static constexpr auto geyser_safe_dist_real = 7.0f;
			static constexpr auto resource_threshold = base_safe_dist + 2.5f;
			// 2020-06-24 AMR NOTE: keeping all distances squared
			static constexpr auto safe_dist = base_safe_dist * base_safe_dist;
			static constexpr auto geyser_safe_dist = geyser_safe_dist_real * geyser_safe_dist_real;
			static constexpr auto threshold = resource_threshold * resource_threshold;
		private:
			uint32_t minerals;
			Cache<UnitTypeData> resource_types;
			Cache<Unit> resources;
			list<tuple<float, float>> base_locations;
			list<Point2D> starts;
		protected:
			Cache<UnitTypeData> basic_base_type;
			Cache<Unit> bases;
			decltype(bases.begin()) first_base;
			bool bases_found;
		public:
			Bases();
		protected:
			void onLoaded() override;
			void onStart(StartRaw const &) override;
			void onObservation(Observation const &) override;
			void onPlayer(PlayerCommon const &) override;
			void onUnitTypeData(UnitTypeData const &) override;
			void onUnit(Unit const &) override;
			void onDeath(uint64_t const &) override;
		protected:
			auto getBases() -> tuple<decltype(bases), decltype(base_locations)>;
			auto buildBase(float const, float const) -> any;
			auto findBases() -> void;
		protected:
			// 2020-06-24 AMR NOTE: actually distance squared
			static auto dist(Unit const &, Point2D const &) -> float;
			static auto dist(Unit const &, Unit const &) -> float;
			static auto dist(Unit const &, tuple<float, float> const &) -> float;
			static auto dist(Point2D const &, tuple<float, float> const &) -> float;
			static auto dist(tuple<float, float> const &, tuple<float, float> const &) -> float;
		protected:
			auto isMyBase(Unit const &) const -> bool;
			auto isMyUnit(Unit const &) const -> bool;
			auto isResource(Unit const &) const -> bool;
			auto isGeyser(Unit const &) const -> bool;
			auto isBasicBaseType(UnitTypeData const &) const -> bool;
			auto isResourceType(UnitTypeData const &) const -> bool;
			auto isStructure(UnitTypeData const &) const -> bool;
			auto isAvailable(UnitTypeData const &) const -> bool;
	};

	Bases::Bases() :
		RawObserver("Bases"s, {"Buildings"s}),
		resource_types(bind(&Bases::isResourceType, this, placeholders::_1)),
		resources(bind(&Bases::isResource, this, placeholders::_1)),
		basic_base_type(bind(&Bases::isBasicBaseType, this, placeholders::_1)),
		bases(bind(&Bases::isMyBase, this, placeholders::_1)),
		bases_found(false)
	{
		add<void>(bind(&Bases::getBases, this));
		add<float, float>(bind(&Bases::buildBase, this, placeholders::_1, placeholders::_2));
	}

	void Bases::onLoaded() {
		app().data()->UnitTypes();
	}

	void Bases::onStart(StartRaw const &start) {
		for(auto const &i : start.start_locations()) {
			COEUS_LOG(Log, Severity::Debug) << "Start locations " << i.x() << " " << i.y();
			starts.push_back(i);
		}
	}

	void Bases::onObservation(Observation const &obs) {
		RawObserver::onObservation(obs);

		if(obs.game_loop() >= 1 && !bases_found) {
			first_base = bases.begin();
			findBases();
			bases_found = !base_locations.empty();
		}
	}

	void Bases::onPlayer(PlayerCommon const &player) {
		if(!player.has_minerals())
			COEUS_LOG(Log, Severity::Error) << "PlayerCommon w/o minerals";
		else
			minerals = player.minerals();
	}

	void Bases::onUnitTypeData(UnitTypeData const &unit) {
		RawObserver::onUnitTypeData(unit);

		if(basic_base_type.insert(unit))
			COEUS_LOG(Log, Severity::Debug) << "Found Base type " << unit.name() << " " << unit.unit_id();
		if(resource_types.insert(unit))
			COEUS_LOG(Log, Severity::Debug) << "Found Resource type " << unit.name() << " " << unit.unit_id();
	}

	void Bases::onUnit(Unit const &unit) {
		RawObserver::onUnit(unit);

		if(bases.insert(unit))
			COEUS_LOG(Log, Severity::Debug) << "Found new base " << unit.tag();

		resources.insert(unit);
	}

	void Bases::onDeath(uint64_t const &unit) {
		RawObserver::onDeath(unit);

		if(bases.remove(unit))
			COEUS_LOG(Log, Severity::Debug) << "Base died";
	}

	auto Bases::getBases() -> tuple<decltype(bases), decltype(base_locations)> {
		return make_tuple(bases, base_locations);
	}

	auto Bases::buildBase(float const xr, float const yr) -> any {
		if(basic_base_type.size() != 1u) {
			COEUS_LOG(Log, Severity::Error) << "Unknown base type: " << basic_base_type.size();
			return any{};
			//COEUS_THROW(Exception("Building base w/o type"s));
		}
		// 2020-07-08 AMR TODO: gas cost?
		if(minerals < basic_base_type.begin()->mineral_cost())
			return any{};

		COEUS_LOG(Log, Severity::Info) << "Attempting to build base at " << xr << "x" << yr;
		// 2020-07-08 AMR TODO: float comparison
		auto i = find_if(base_locations.begin(), base_locations.end(), [&xr, &yr](auto const &l) -> bool {
			return get<0>(l) == xr && get<1>(l) == yr;
		});

		// 2020-07-08 AMR TODO: order by priority
		if(i == base_locations.end()) {
			i = base_locations.begin();
		}

		auto const [x, y] = *i;
		auto res = any{};
		try {
			res = dep("Buildings")->call(
				basic_base_type.begin()->unit_id(),
				*i, *i,
				function<void(uint64_t const)>([this, x, y](uint64_t const id) {
					COEUS_LOG(Log, Severity::Info) << "base (" << id << ") finished at " << x << "x" << y;
				})
			);

			if(!res.has_value()) {
				COEUS_LOG(Log, Severity::Debug) << "didn't get a value from Buildings this shouldn't be possible";
			}
		} catch(bad_any_cast &) {
			COEUS_LOG(Log, Severity::Error) << "Buildings call failed to split/cast params";
		}
		try {
			if(!res.has_value()) {
				COEUS_LOG(Log, Severity::Error) << "res not set; did the above throw?";
			} else
			if(any_cast<bool>(res)) {
				base_locations.erase(i);
				return make_tuple(x, y);
			} else {
				return any{};
			}
		} catch(bad_any_cast &) {
			COEUS_LOG(Log, Severity::Error) << "Something is wrong w/ value from Buildings " << detail::demangle(res.type().name());
		}

		return any{};
	}

	auto Bases::findBases() -> void {
		if(first_base == bases.end())
			COEUS_THROW(Exception("I don't know my first base"s));

		auto const check_proximity = [this](auto const &a, auto const &b) -> bool {
			return dist(a, b) < threshold;
		};
		auto const merge = [](auto &to, auto &from) {
			for(auto &i : from) {
				to.insert(i);
			}
		};
		auto const centroid = [](auto const &points) -> tuple<float, float> {
			auto const n = points.size();
			auto const x = accumulate(points.begin(), points.end(), 0.0f, [](auto const &v, auto const &unit) -> float {
				return v + unit.pos().x();
			});
			auto const y = accumulate(points.begin(), points.end(), 0.0f, [](auto const &v, auto const &unit) -> float {
				return v + unit.pos().y();
			});
			return make_tuple(x / static_cast<float>(n), y / static_cast<float>(n));
		};
		auto fields = list<Cache<Unit>>{};

		COEUS_LOG(Log, Severity::Debug) << "resource count " << resources.size();
		for(auto &r : resources) {
			fields.emplace_back(initializer_list{r});
		}
		COEUS_LOG(Log, Severity::Debug) << "pre merge size " << fields.size();

		auto merged = true;
		while(merged) {
			merged = false;
			for(auto i = fields.begin(); i != fields.end(); i++) {
				auto j = i;
				j++;
				for(; j != fields.end(); j++) {
					// 2020-07-21 AMR XXX: check merged first becuase l will be destructed in that case
					for(auto k = i->begin(); !merged && k != i->end(); k++) {
						for(auto l = j->begin(); !merged && l != j->end(); l++) {
							if(check_proximity(*k, *l)) {
								merge(*i, *j);

								// 2020-07-21 AMR NOTE: decrement back to previous so next will be after erase
								auto old = j--;
								fields.erase(old);

								merged = true;
								// 2020-07-20 AMR XXX: erase invalidated old and l so need to break
								break;
							}
						}
					}
				}
			}
		}

		auto const safe = [this](auto const &fields, auto const &loc) -> bool {
			for(auto &i : fields) {
				if(isGeyser(i) && dist(i, loc) < geyser_safe_dist) {
					return false;
				} else if(dist(i, loc) < safe_dist)
					return false;
			}

			return true;
		};

		COEUS_LOG(Log, Severity::Debug) << "post merge size " << fields.size();
		for(auto &f : fields) {
			auto &&loc = centroid(f);
			COEUS_LOG(Log, Severity::Debug) << "found center of mineral field: " << get<0>(loc) << " " << get<1>(loc);

			auto safes = list<tuple<float, float>>{};
			// 2020-06-24 AMR NOTE: recenter to where base goes
			for(auto x = get<0>(loc) - base_safe_dist; x <= get<0>(loc) + base_safe_dist; x++) {
				for(auto y = get<1>(loc) - base_safe_dist; y <= get<1>(loc) + base_safe_dist; y++) {
					auto l = make_tuple(floor(x), floor(y));
					if(
						any_cast<bool>(dep("Buildings"s)->call(basic_base_type.begin()->unit_id(), make_tuple(x, y))) &&
						safe(f, l)
					) {
						safes.emplace_back(move(l));
					}
				}
			}

			auto last = safes.end();
			for(auto i = safes.begin(); i != safes.end(); i++) {
				if(last == safes.end() || dist(loc, *i) < dist(loc, *last))
					last = i;
			}

			if(last == safes.end()) {
				COEUS_LOG(Log, Severity::Error) << "No safe build location for " << get<0>(loc) << " " << get<1>(loc);
				//base_locations.emplace_back(move(loc));
			} else {
				COEUS_LOG(Log, Severity::Debug) << "found base location " << get<0>(*last) << " " << get<1>(*last);
				base_locations.emplace_back(move(*last));
			}
		}

		COEUS_LOG(Log, Severity::Debug) << "finding my and op fields";
		// 2020-06-24 AMR TODO: support multiple opponents
		auto my = base_locations.end();
		auto op = base_locations.end();
		for(auto i = base_locations.begin(); i != base_locations.end(); i++) {
			if(my == base_locations.end() || dist(*first_base, *i) < dist(*first_base, *my))
				my = i;
			if(op == base_locations.end() || dist(starts.front(), *i) < dist(starts.front(), *op))
				op = i;
		}

		if(my == base_locations.end()) {
			COEUS_LOG(Log, Severity::Error) << "Failed to find my main base";
		} else {
			COEUS_LOG(Log, Severity::Debug) << "my main base mineral field " << get<0>(*my) << " " << get<1>(*my);
			base_locations.erase(my);
		}

		if(op == base_locations.end()) {
			COEUS_LOG(Log, Severity::Error) << "Failed to find opponents main base";
		} else {
			COEUS_LOG(Log, Severity::Debug) << "opponent main base mineral field " << get<0>(*op) << " " << get<1>(*op);
			base_locations.erase(op);
		}

		for(auto &i : base_locations) {
			COEUS_LOG(Log, Severity::Info) << "found base location " << get<0>(i) << " " << get<1>(i);
		}
	}

	auto Bases::dist(Unit const &unit, Point2D const &point) -> float {
		return pow(unit.pos().x() - point.x(), 2.0f) + pow(unit.pos().y() - point.y(), 2.0f);
	}

	auto Bases::dist(Unit const &a, Unit const &b) -> float {
		return pow(a.pos().x() - b.pos().x(), 2.0f) + pow(a.pos().y() - b.pos().y(), 2.0f);
	}

	auto Bases::dist(Unit const &unit, tuple<float, float> const &tup) -> float {
		return pow(unit.pos().x() - get<0>(tup), 2.0f) + pow(unit.pos().y() - get<1>(tup), 2.0f);
	}

	auto Bases::dist(Point2D const &point, tuple<float, float> const &tup) -> float {
		return pow(point.x() - get<0>(tup), 2.0f) + pow(point.y() - get<1>(tup), 2.0f);
	}

	auto Bases::dist(tuple<float, float> const &a, tuple<float, float> const &b) -> float {
		return pow(get<0>(a) - get<0>(b), 2.0f) + pow(get<1>(a) - get<1>(b), 2.0f);
	}

	auto Bases::isMyBase(Unit const &data) const -> bool {
		if(basic_base_type.size() == 0u)
			COEUS_THROW(Exception("looking for base without type"s));

		return
			isMyUnit(data) &&
			data.has_unit_type() &&
			find_if(basic_base_type.begin(), basic_base_type.end(), [data](auto &i) -> bool {
				return data.unit_type() == i.unit_id();
			}) != basic_base_type.end()
		;
	}

	auto Bases::isMyUnit(Unit const &data) const -> bool {
		return data.has_owner() && static_cast<uint32_t>(data.owner()) == id();
	}

	auto Bases::isResource(Unit const &data) const -> bool {
		// 2020-06-20 AMR NOTE: don't include mineral path blockers in this
		return
			resource_types.find(data.unit_type()) != resource_types.end() && (
				data.has_mineral_contents() && (
					data.mineral_contents() >= 100 ||
					data.mineral_contents() == 0
				) ||
				!data.has_mineral_contents()
			)
		;
	}

	auto Bases::isGeyser(Unit const &unit) const -> bool {
		return
			unit.has_vespene_contents() &&
			unit.vespene_contents() > 0
		;
	}

	auto Bases::isBasicBaseType(UnitTypeData const &data) const -> bool {
		return 
			isStructure(data) &&
			data.has_name() && (
				data.name() == "Hatchery"s
			)
		;
	}

	auto Bases::isResourceType(UnitTypeData const &data) const -> bool {
		return
			isAvailable(data) && (
				data.has_has_vespene() && data.has_vespene() ||
				data.has_has_minerals() && data.has_minerals()
			)
		;
	}

	auto Bases::isStructure(UnitTypeData const &data) const -> bool {
		if(isAvailable(data))
			for(auto &i : data.attributes())
				if(i == Structure)
					return true;

		return false;
	}

	auto Bases::isAvailable(UnitTypeData const &data) const -> bool {
		return data.has_available() && data.available();
	}

	COEUS_MODULE(Bases)
}
