/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "../src/RawObserver.hh"
#include "../src/Cache.hh"

using namespace std;
using namespace Sc2Ai;

using ::SC2APIProtocol::Structure;

#	define GET_OR_LOG(obj, field, res) \
		if(!obj.has_ ## field()) COEUS_LOG(Log, Severity::Error) << "failed to find field " # field; \
		else res = obj.field()

namespace {
	class Workers :
		public RawObserver
	{
		// 2020-05-31 AMR TODO: private?
		protected:
			size_t limit;
			uint32_t minerals;
			uint32_t food_cap;
			uint32_t food_used;
			Cache<UnitTypeData> worker_type;
			Cache<Unit> workers;
			list<uint64_t> busy_workers;
		public:
			Workers();
			auto busyWorker() -> any;
			auto idleWorker(uint64_t const) -> void;
		protected:
			void onLoaded() override;
			void onObservation(Observation const &) override;
			void onPlayer(PlayerCommon const &) override;
			void onUnitTypeData(UnitTypeData const &) override;
			void onUnit(Unit const &) override;
			void onDeath(uint64_t const &) override;
		protected:
			auto queueWorkers() -> void;
			auto workerCount() const -> size_t;
			auto basesCount() -> size_t;
			constexpr auto idealWorkersPerBase() const -> size_t;
			auto workerSoftCap() -> size_t;
			auto workerHardCap() const -> size_t;
		protected:
			auto isMyWorker(Unit const &) const -> bool;
			auto isMyUnit(Unit const &) const -> bool;
			auto isMyWorkerType(UnitTypeData const &) const -> bool;
			auto isWorkerType(UnitTypeData const &) const -> bool;
			auto isMyRace(UnitTypeData const &) const -> bool;
			auto isStructure(UnitTypeData const &) const -> bool;
			auto isAvailable(UnitTypeData const &) const -> bool;
	};

	Workers::Workers() :
		RawObserver("Workers"s, {"Bases"s, "Train"s, "Supply"s}),
		limit(80u),
		minerals(0u),
		food_cap(0u),
		food_used(200u),
		worker_type(bind(&Workers::isMyWorkerType, this, placeholders::_1)),
		workers(bind(&Workers::isMyWorker, this, placeholders::_1))
	{
		add<void>(bind(&Workers::busyWorker, this));
		add<uint64_t>(bind(&Workers::idleWorker, this, placeholders::_1));
	}

	auto Workers::busyWorker() -> any {
		COEUS_LOG(Log, Severity::Debug) << "worker requested";
		auto i = find_if(workers.begin(), workers.end(), [this](auto const &unit) -> bool {
			return find(busy_workers.begin(), busy_workers.end(), unit.tag()) == busy_workers.end();
		});
		if(i == workers.end()) {
			COEUS_LOG(Log, Severity::Error) << "couldn't find a non-busy worker";
			return any{};
		}

		COEUS_LOG(Log, Severity::Debug) << "found worker " << i->tag();
		busy_workers.push_back(i->tag());
		return i->tag();
	}

	auto Workers::idleWorker(uint64_t const param) -> void {
		COEUS_LOG(Log, Severity::Debug) << "worker " << param << " returned";
		auto i = find(busy_workers.begin(), busy_workers.end(), any_cast<uint64_t>(param));
		busy_workers.erase(i);
	}

	void Workers::onLoaded() {
		app().data()->UnitTypes();
	}

	void Workers::onObservation(Observation const &obs) {
		RawObserver::onObservation(obs);

		// 2020-05-22 AMR TODO: consider minerals and supply
		if(workerCount() < workerHardCap() && workerCount() < workerSoftCap()) {
			queueWorkers();
		}
	}

	void Workers::onPlayer(PlayerCommon const &player) {
		GET_OR_LOG(player, minerals, minerals);
		GET_OR_LOG(player, food_cap, food_cap);
		GET_OR_LOG(player, food_used, food_used);
	}

	void Workers::onUnitTypeData(UnitTypeData const &unit) {
		if(worker_type.insert(unit))
			COEUS_LOG(Log, Severity::Debug) << "Foound worker type " << unit.unit_id();
	}

	void Workers::onUnit(Unit const &unit) {
		RawObserver::onUnit(unit);

		if(workers.insert(unit))
			COEUS_LOG(Log, Severity::Debug) << "Found new worker " << unit.tag();
	}

	void Workers::onDeath(uint64_t const &unit) {
		RawObserver::onDeath(unit);

		COEUS_LOG(Log, Severity::Debug) << "unit died";

		if(workers.remove(unit))
			COEUS_LOG(Log, Severity::Debug) << "Worker Died " << unit;

		auto const i = find(busy_workers.begin(), busy_workers.end(), unit);
		if(i != busy_workers.end()) {
			COEUS_LOG(Log, Severity::Debug) << "Worker " << unit << " was busy when it died";
			busy_workers.erase(i);
		}
	}

	// 2020-05-17 AMR TODO: track idle?
	// 2020-05-31 AMR TODO: track larvae?
	auto Workers::queueWorkers() -> void {
		if(
			minerals >= worker_type.begin()->mineral_cost() &&
			food_used + worker_type.begin()->food_required() <= food_cap
		) {
			auto r = any_cast<bool>(dep("Train"s)->call(worker_type.begin()->unit_id()));
			if(r)
				COEUS_LOG(Log, Severity::Debug) << "Training worker ";
		}
	}

	auto Workers::workerCount() const -> size_t {
		return workers.size();
	}

	// 2020-05-10 AMR TODO: don't count macro hatches, etc
	auto Workers::basesCount() -> size_t {
		// 2020-07-08 AMR TODO: this is pretty cancer
		return get<0>(any_cast<tuple<Cache<Unit>, list<tuple<float, float>>>>(dep("Bases")->call())).size();
	}

	// 2020-05-10 AMR TODO: per base situation
	// 2020-05-10 AMR TODO: ML model?
	// 2020-05-31 AMR TODO: can get from base/gas units
	constexpr auto Workers::idealWorkersPerBase() const -> size_t {
		constexpr auto nodes = 8;
		constexpr auto gas = 0;		// 2020-05-10 AMR TODO: skipping gas for now
		constexpr auto per_node = 2;
		constexpr auto per_gas = 3;
		// 2020-05-10 AMR TODO: a good value for this?
		// 2020-05-11 AMR TODO: a better place for this
		constexpr auto buff_amount = 6;

		return gas * per_gas + nodes * per_node + buff_amount;
	}

	auto Workers::workerSoftCap() -> size_t {
		return basesCount() * idealWorkersPerBase();
	}

	auto Workers::workerHardCap() const -> size_t {
		return limit;
	}

	auto Workers::isMyWorker(Unit const &data) const -> bool {
		if(worker_type.size() != 1u)
			COEUS_THROW(Exception("Looking for Worker without Worker Type"s));

		return isMyUnit(data) && data.has_unit_type() && data.unit_type() == worker_type.begin()->unit_id();
	}

	auto Workers::isMyUnit(Unit const &data) const -> bool {
		return data.has_owner() && static_cast<uint32_t>(data.owner()) == id();
	}

	auto Workers::isMyWorkerType(UnitTypeData const &data) const -> bool {
		return isMyRace(data) && isWorkerType(data);
	}

	auto Workers::isWorkerType(UnitTypeData const &data) const -> bool {
		return isAvailable(data) && !isStructure(data) && data.has_name() && (
			data.name() == "SCV"s ||
			data.name() == "Drone"s ||
			data.name() == "Probe"s
		);
	}

	auto Workers::isStructure(UnitTypeData const &data) const -> bool {
		if(isAvailable(data))
			for(auto &i : data.attributes())
				if(i == Structure)
					return true;

		return false;
	}

	auto Workers::isMyRace(UnitTypeData const &data) const -> bool {
		return race() != NoRace && isAvailable(data) && data.has_race() && data.race() == race();
	}

	auto Workers::isAvailable(UnitTypeData const &data) const -> bool {
		return data.has_available() && data.available();
	}

	COEUS_MODULE(Workers)
}
