/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "../src/RawObserver.hh"
#include "../src/Cache.hh"

using namespace std;
using namespace Sc2Ai;

using ::SC2APIProtocol::Zerg;
using ::SC2APIProtocol::Structure;

namespace {
	// 2020-07-30 AMR TODO: proximity to task
	// 2020-07-30 AMR TODO: actually creep
	class ZergMacro :
		public RawObserver
	{
		private:
			// 2020-07-30 AMR TODO: flexible?
			static constexpr auto creep_queen_count = 4u;
			uint32_t minerals;
			Cache<UnitTypeData> spawning_pool_type;
			Cache<Unit> pool;
			Cache<UnitTypeData> queen_type;
			Cache<Unit> queens;
			bool pool_on_way;
		public:
			ZergMacro();
		protected:
			void onLoaded() override;
			void onUnitTypeData(UnitTypeData const &) override;
			void onObservation(Observation const &) override;
			void onPlayer(PlayerCommon const &) override;
			void onUnit(Unit const &) override;
			void onDeath(uint64_t const &) override;
		protected:
			auto getQueens() -> decltype(queens) &;
			auto getBases() -> Cache<Unit>;
			auto getBaseLocations() -> list<tuple<float, float>>;
			auto buildPool() -> void;
			auto buildQueen() -> void;
		private:
			auto isQueen(Unit const &) const -> bool;
			auto isSpawningPool(Unit const &) const -> bool;
			auto isQueenType(UnitTypeData const &) const -> bool;
			auto isSpawningPoolType(UnitTypeData const &) const -> bool;
			auto isZerg(UnitTypeData const &) const -> bool;
			auto isStructure(UnitTypeData const &) const -> bool;
			auto isAvailable(UnitTypeData const &) const -> bool;
	};

	ZergMacro::ZergMacro() :
		RawObserver("ZergMacro"s, {"Bases"s, "Buildings"s, "Train"s}),
		minerals(0u),
		spawning_pool_type(bind(&ZergMacro::isSpawningPoolType, this, placeholders::_1)),
		pool(bind(&ZergMacro::isSpawningPool, this, placeholders::_1)),
		queen_type(bind(&ZergMacro::isQueenType, this, placeholders::_1)),
		queens(bind(&ZergMacro::isQueen, this, placeholders::_1)),
		pool_on_way(false)
	{
		add<void>(bind(&ZergMacro::getQueens, this));
	}

	void ZergMacro::onLoaded() {
		app().data()->UnitTypes();

		RawObserver::onLoaded();
	}

	void ZergMacro::onUnitTypeData(UnitTypeData const &data) {
		queen_type.insert(data);
		spawning_pool_type.insert(data);

		RawObserver::onUnitTypeData(data);
	}

	void ZergMacro::onObservation(Observation const &obs) {
		if(queen_type.size() != 1u)
			COEUS_THROW(Exception("Queen type not found Or found multiple queen types"s));
		if(spawning_pool_type.size() != 1u)
			COEUS_THROW(Exception("Spawning pool type not found Or found multiple spawning pool types"s));

		if(pool.size() < 1u) {
			buildPool();
		} else if(getBases().size() + creep_queen_count > getQueens().size()) {
			// 2020-08-03 AMR TODO: macro hatcheries
			buildQueen();
		}

		RawObserver::onObservation(obs);
	}

	void ZergMacro::onPlayer(PlayerCommon const &player) {
		if(!player.has_minerals())
			COEUS_LOG(Log, Severity::Error) << "failed to get minerals from player";
		else
			minerals = player.minerals();

		Observer::onPlayer(player);
	}

	void ZergMacro::onUnit(Unit const &data) {
		if(queens.insert(data)) {
			COEUS_LOG(Log, Severity::Debug) << "Found new queen " << data.tag();
		}
		if(pool.insert(data)) {
			COEUS_LOG(Log, Severity::Debug) << "Found new pool " << data.tag();
		}

		RawObserver::onUnit(data);
	}

	void ZergMacro::onDeath(uint64_t const &id) {
		if(queens.remove(id)) {
			COEUS_LOG(Log, Severity::Debug) << "Queen died " << id;
		}
		if(pool.remove(id)) {
			COEUS_LOG(Log, Severity::Debug) << "Pool died " << id;
		}

		RawObserver::onDeath(id);
	}

	auto ZergMacro::getQueens() -> decltype(queens) & {
		return queens;
	}

	auto ZergMacro::getBases() -> Cache<Unit> {
		auto r = dep("Bases"s)->call();
		if(!r.has_value())
			COEUS_THROW(Exception("Failed to get bases from Bases module"s));

		try {
			return get<0>(any_cast<tuple<Cache<Unit>, list<tuple<float, float>>>>(r));
		} catch(bad_any_cast &) {
			// 2020-08-03 AMR TODO: abstract?
			// 2020-08-03 AMR TODO: include base exception?
			COEUS_THROW(Exception("Got invalid type from Bases->call()"s));
		}
	}

	auto ZergMacro::getBaseLocations() -> list<tuple<float, float>> {
		auto r = dep("Bases"s)->call();
		if(!r.has_value())
			COEUS_THROW(Exception("Failed to get base locations from Bases module"s));

		try {
			return get<1>(any_cast<tuple<Cache<Unit>, list<tuple<float, float>>>>(r));
		} catch(bad_any_cast &) {
			COEUS_THROW(Exception("Got invalid type from Bases->call()"s));
		}
	}

	auto ZergMacro::buildPool() -> void {
		if(spawning_pool_type.size() < 1u)
			COEUS_THROW(Exception("I don't know which building is a spawning pool"s));
		if(pool_on_way) {
			COEUS_LOG(Log, Severity::Debug) << "spawning pool on way; not building; (hopefully)";
			return;
		}
		if(minerals < spawning_pool_type.begin()->mineral_cost()) {
			COEUS_LOG(Log, Severity::Debug) << "no minerals for pool";
			return;
		}

		auto r = dep("Buildings"s)->call(
			spawning_pool_type.begin()->unit_id(),
			make_tuple(getBases().begin()->pos().x(), getBases().begin()->pos().y()),
			getBaseLocations().front(),
			function<void(uint64_t const)>([this](uint64_t const id) {
				COEUS_LOG(Log, Severity::Info) << "Spawning Pool built " << id;
				pool_on_way = false;
			})
		);
		// 2020-08-03 AMR TODO: abstract
		if(!r.has_value())
			COEUS_THROW(Exception("Failed to build spawning pool w/ Buildings module"s));

		auto v = false;
		try {
			v = any_cast<bool>(r);
		} catch(bad_any_cast &) {
			COEUS_THROW(Exception("Got an invalid type from Buildings->call(Id, Pos, Pos, Cb)"s));
		}

		pool_on_way = true;
		if(!v) {
			COEUS_LOG(Log, Severity::Error) << "Failed to build spawning pool";
		}
	}

	auto ZergMacro::buildQueen() -> void {
		auto r = dep("Train"s)->call(queen_type.begin()->unit_id());
		
		try {
			if(!r.has_value() || !any_cast<bool>(r)) {
				COEUS_LOG(Log, Severity::Error) << "Failed to build queen"s;
			}
		} catch(bad_any_cast &) {
			COEUS_THROW(Exception("Train->call(queen_type) returned invalid type"s));
		}
	}

	auto ZergMacro::isQueen(Unit const &data) const -> bool {
		if(queen_type.size() != 1u)
			COEUS_THROW(Exception("Queen type not found before units Or found more than 1 queen type"s));

		return
			data.has_unit_type() && data.unit_type() == queen_type.begin()->unit_id()
		;
	}

	auto ZergMacro::isSpawningPool(Unit const &data) const -> bool {
		if(spawning_pool_type.size() != 1u)
			COEUS_THROW(Exception("Pool type not found before units Or found more than 1 pool type"s));

		return
			data.has_unit_type() && data.unit_type() == spawning_pool_type.begin()->unit_id()
		;
	}

	auto ZergMacro::isQueenType(UnitTypeData const &data) const -> bool {
		return
			!isStructure(data) &&
			isZerg(data) &&
			data.has_name() && data.name() == "Queen"s
		;
	}

	auto ZergMacro::isSpawningPoolType(UnitTypeData const &data) const -> bool {
		return
			isStructure(data) &&
			isZerg(data) &&
			data.has_name() && data.name() == "SpawningPool"s
		;
	}

	auto ZergMacro::isZerg(UnitTypeData const &data) const -> bool {
		return
			data.has_race() && data.race() == Zerg
		;
	}

	auto ZergMacro::isStructure(UnitTypeData const &data) const -> bool {
		if(isAvailable(data))
			for(auto &i : data.attributes())
				if(i == Structure)
					return true;

		return false;
	}

	auto ZergMacro::isAvailable(UnitTypeData const &data) const -> bool {
		return data.has_available() && data.available();
	}

	COEUS_MODULE(ZergMacro)
}
