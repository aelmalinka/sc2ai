/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "../src/RawObserver.hh"
#include "../src/Cache.hh"
#include <list>

using namespace std;
using namespace Sc2Ai;

using ::SC2APIProtocol::Race;
using ::SC2APIProtocol::NoRace;
using ::SC2APIProtocol::Terran;
using ::SC2APIProtocol::Zerg;
using ::SC2APIProtocol::Protoss;

#	define GET_OR_LOG(obj, field, res) \
		if(!obj.has_ ## field()) COEUS_LOG(Log, Severity::Error) << "failed to find field " # field; \
		else res = obj.field()

namespace {
	class Supply :
		public RawObserver
	{
		private:
			Cache<UnitTypeData> supply_source;
			Cache<Unit> supply;
			list<bool> pendings;
			uint32_t cap;
			uint32_t used;
			uint32_t minerals;
		public:
			Supply();
		protected:
			any defCall(any const &) final;
			void onLoaded() override;
			void onUnitTypeData(UnitTypeData const &) override;
			void onPlayer(PlayerCommon const &) override;
			void onObservation(Observation const &) override;
			void onUnit(Unit const &) override;
			void onDeath(uint64_t const &) override;
		protected:
			auto pending() const -> uint32_t;
			auto buildSupply(uint32_t const) -> uint32_t;
		protected:
			auto isSupply(Unit const &) const -> bool;
			auto isMySupply(UnitTypeData const &) const -> bool;
			auto isMyRace(UnitTypeData const &) const -> bool;
			template<typename T>
			auto isAvailable(T const &) const -> bool;
	};

	Supply::Supply() :
		RawObserver("Supply"s, {"Train"s}),
		supply_source(bind(&Supply::isMySupply, this, placeholders::_1)),
		supply(bind(&Supply::isSupply, this, placeholders::_1)),
		cap(0u),
		used(0u),
		minerals(0u)
	{}

	any Supply::defCall(any const &param) {
		if(!param.has_value())
			return cap;

		return buildSupply(any_cast<uint32_t>(param));
	}

	void Supply::onLoaded() {
		app().data()->UnitTypes();
	}

	void Supply::onUnitTypeData(UnitTypeData const &data) {
		RawObserver::onUnitTypeData(data);

		if(supply_source.insert(data))
			COEUS_LOG(Log, Severity::Debug) << "Found my supply unit " << data.unit_id();
	}

	void Supply::onPlayer(PlayerCommon const &player) {
		Observer::onPlayer(player);

		GET_OR_LOG(player, minerals, minerals);
		GET_OR_LOG(player, food_cap, cap);
		GET_OR_LOG(player, food_used, used);
	}

	void Supply::onObservation(Observation const &obs) {
		RawObserver::onObservation(obs);

		// 2020-06-03 AMR NOTE: checks in_progress and pendings
		if(used >= cap + pending())
			buildSupply(cap + pending() + 1);
	}

	void Supply::onUnit(Unit const &unit) {
		RawObserver::onUnit(unit);

		if(supply.insert(unit)) {
			COEUS_LOG(Log, Severity::Debug) << "Found new supply " << unit.tag();
			if(!pendings.empty())
				pendings.pop_front();
		}
	}

	void Supply::onDeath(uint64_t const &unit) {
		RawObserver::onDeath(unit);
		
		if(supply.remove(unit))
			COEUS_LOG(Log, Severity::Debug) << "Lost supply unit " << unit;

		// 2020-06-04 AMR TODO: detect egg/overlord/etc?
	}

	// 2020-06-01 AMR TODO: will this ever need to be more sophisticated?
	auto Supply::pending() const -> uint32_t {
		auto const per = supply_source.begin()->food_provided();
		return per * pendings.size();
	}

	auto Supply::buildSupply(uint32_t const target) -> uint32_t {
		if(supply_source.size() != 1u)
			COEUS_THROW(Exception("making supply w/o supply data"s));

		if(
			target >= cap + pending() &&
			minerals >= supply_source.begin()->mineral_cost()
		) {
			COEUS_LOG(Log, Severity::Debug) << "requested " << target << " at " << cap << " building (using " << used << ")";
			if(race() == Zerg && any_cast<bool>(dep("Train"s)->call(supply_source.begin()->unit_id())))
				pendings.push_back(true);
		}

		return cap + pending();
	}

	auto Supply::isSupply(Unit const &data) const -> bool {
		return
			data.has_unit_type() &&
			supply_source.begin() != supply_source.end() &&
			data.unit_type() == supply_source.begin()->unit_id()
		;
	}

	auto Supply::isMySupply(UnitTypeData const &data) const -> bool {
		return
			isAvailable(data) &&
			isMyRace(data) &&
			data.has_name() && (
				data.name() == "Overlord"s ||
				data.name() == "SupplyDepot"s ||
				data.name() == "Pylon"s
			)
		;
	}

	auto Supply::isMyRace(UnitTypeData const &data) const -> bool {
		return race() != NoRace && data.has_race() && data.race() == race();
	}

	template<typename T>
	auto Supply::isAvailable(T const &data) const -> bool {
		return data.has_available() && data.available();
	}

	COEUS_MODULE(Supply)
}
