/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_MODULE_INC
#	define SC2AI_MODULE_INC

#	include <any>
#	include "Exception.hh"
#	include "Sc2Api.hh"
#	include "Responder.hh"
#	include "Visitor.hh"
#	include <Coeus/Import.hh>

	namespace Sc2Ai
	{
		class Application;

		COEUS_ERROR_INFO(ModuleName, std::string);

		class Module :
			public Responder
		{
			private:
				std::string _name;
				std::list<std::string> _dep_names;
				std::shared_ptr<Sc2Api> _api;
				std::shared_ptr<Application> _app;
				std::map<std::string, Coeus::Import<Module>> _deps;
				Visitor _vis;
			protected:
				mutable Coeus::Log::Source Log;
				using name_type = decltype(_name);
				using dep_names_type = decltype(_dep_names);
			public:
				explicit Module(
					name_type const &,
					dep_names_type const & = {}
				);
				virtual ~Module();
				auto setApi(decltype(_api) const &) -> void;
				auto setApplication(decltype(_app) const &) -> void;
				auto name() const -> name_type const &;
			public:
				virtual void onLoaded() {}
				template<typename ...Args>
				auto call(Args && ...) const -> std::any;
			protected:
				virtual std::any defCall(std::any const &);
				virtual void onError(decltype(std::declval<Response>().error()) const &);
			protected:
				template<typename ...Args, typename F>
				auto add(F &&) -> void;
				auto app() -> decltype(*_app) &;
				auto api() -> decltype(*_api) &;
				auto dep(std::string const &) -> decltype(_deps)::mapped_type &;
			private:
				auto load_dep(std::string const &) -> void;
				auto save_dep(decltype(_deps)::mapped_type const &) -> void;
		};
	}

#	include "Application.hh"
#	include "Module.impl.hh"

#endif
