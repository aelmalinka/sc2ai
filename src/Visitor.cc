/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Visitor.hh"

using namespace Sc2Ai;
using namespace std;

// 2020-06-24 AMR TODO: is there a clang supported version? is this it?
#ifdef __GNUG__
#	include <memory>
#	include <cxxabi.h>
	namespace Sc2Ai { namespace detail {
		string demangle(const char *name) {
			int status = -1;

			unique_ptr<char, void(*)(void *)> res {
				abi::__cxa_demangle(name, NULL, NULL, &status),
				free
			};

			return (status == 0) ? res.get() : name;
		}
	} }
#	else
	namespace Sc2Ai { namespace detail {
		string demangle(const char *name) {
			return name;
		}
	} }
#endif

Visitor::Visitor() = default;

Visitor::Visitor(f_type const &f) :
	_def(f)
{}

auto Visitor::setDefault(f_type const &f) -> void {
	_def = f;
}

auto Visitor::clearDefault() -> void {
	_def = function<any(any const &)>{};
}

auto Visitor::call(any const &a) const -> any {
	if(auto const i = _visits.find(type_index(a.type())); i != _visits.end())
		return i->second(a);
	else if(_def)
		return _def(a);
	else
		COEUS_THROW(Exception("Unknown type in visitation")
			<< AnyInfo(a)
			<< TypeInfo(a.type())
		);
}
