/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_OBSERVER_INC
#	define SC2AI_OBSERVER_INC

#	include "Module.hh"

	namespace Sc2Ai
	{
		using ::SC2APIProtocol::Action;
		using ::SC2APIProtocol::ActionError;
		using ::SC2APIProtocol::Observation;
		using ::SC2APIProtocol::PlayerResult;
		using ::SC2APIProtocol::ChatReceived;
		using ::SC2APIProtocol::PlayerCommon;
		using ::SC2APIProtocol::AbilityData;
		using ::SC2APIProtocol::UnitTypeData;
		using ::SC2APIProtocol::UpgradeData;
		using ::SC2APIProtocol::BuffData;
		using ::SC2APIProtocol::EffectData;

		class Observer :
			public Module
		{
			private:
				std::uint32_t _id;
				bool _ready;
				std::queue<ResponseObservation> _obs;
			public:
				Observer(name_type const &, dep_names_type const &);
				virtual ~Observer();
			protected:
				virtual void onJoinGame(ResponseJoinGame const &) override;
				virtual void onObservation(ResponseObservation const &) override;
				virtual void onData(ResponseData const &) override;
			protected:
				virtual void onAction(Action const &) {}
				virtual void onActionError(ActionError const &) {}
				virtual void onObservation(Observation const &);
				virtual void onPlayerResult(PlayerResult const &) {}
				virtual void onChatReceived(ChatReceived const &) {}
				virtual void onPlayer(PlayerCommon const &) {}
				virtual void onAbilityData(AbilityData const &) {}
				virtual void onUnitTypeData(UnitTypeData const &) {}
				virtual void onUpgradeData(UpgradeData const &) {}
				virtual void onBuffData(BuffData const &) {}
				virtual void onEffectData(EffectData const &) {}
			protected:
				auto doAction(std::vector<Action> &&) -> void;
				auto id() const -> decltype(_id) const &;
		};
	}

#endif
