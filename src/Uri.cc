/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "Uri.hh"
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>

using namespace Sc2Ai;
using namespace std;

namespace qi = boost::spirit::qi;

namespace {
	template<typename Iterator>
	class allchars :
		public qi::grammar<Iterator, string()>
	{
		private:
			qi::rule<Iterator, string()> start;
			qi::rule<Iterator, string()> extra;
		public:
			allchars() :
				allchars::base_type(start)
			{
				using qi::char_;
				using qi::alpha;
				using qi::alnum;

				extra =
					char_('$') |
					char_('-') |
					char_('_') |
					char_('@') |
					char_('.') |
					char_('&') |
					char_('+') |
					char_('=') |
					char_('!') |
					char_('*') |
					char_('"') |
					char_('\'') |
					char_('(') |
					char_(')') |
					char_(',') |
					char_('%')
				;

				start =
					alnum |
					extra
				;
			}
	};
	template<typename Iterator>
	class authparser :
		public qi::grammar<Iterator, Uri::Authority()>
	{
		private:
			qi::rule<Iterator, Uri::Authority()> start;
			allchars<Iterator> all;
			qi::rule<Iterator, string()> user;
			qi::rule<Iterator, string()> host;
			qi::rule<Iterator, string()> port;
		public:
			authparser() :
				authparser::base_type(start)
			{
				using qi::lit;
				using qi::digit;
				using qi::attr;
				using qi::_val;
				using qi::_1;

				user =
					*all >>
					lit('@')
				;
				// 2020-04-21 AMR TODO: ipv6 addresses "[::1]"
				host =
					*all
				;
				port =
					lit(':') >>
					*digit
				;

				start =
					attr(Uri::Authority()) >>
					-user[bind(&Uri::Authority::setUser, _val, _1)] >>
					host[bind(&Uri::Authority::setHost, _val, _1)] >>
					-port[bind(&Uri::Authority::setPort, _val, _1)]
				;
			}
	};

	template<typename Iterator>
	class parser :
		public qi::grammar<Iterator, Uri()>
	{
		private:
			qi::rule<Iterator, Uri()> start;
			authparser<Iterator> auth;
			allchars<Iterator> all;
			qi::rule<Iterator, string()> allpath;
			qi::rule<Iterator, string()> scheme;
			qi::rule<Iterator, string()> path;
			qi::rule<Iterator, string()> query;
			qi::rule<Iterator, string()> frag;
		public:
			parser() :
				parser::base_type(start)
			{
				using qi::lit;
				using qi::char_;
				using qi::alpha;
				using qi::attr;
				using qi::_val;
				using qi::_1;

				allpath =
					all |
					char_('/')
				;

				scheme =
					alpha >>
					*all
				;
				path =
					+allpath
				;
				query =
					*all
				;
				frag =
					*all
				;

				start =
					qi::attr(Uri()) >>
					scheme[bind(&Uri::setScheme, _val, _1)] >> lit(':') >>
					-( lit("//"s) >> auth[bind(&Uri::setAuthority, _val, _1)] ) >>
					path[bind(&Uri::setPath, _val, _1)] >>
					-( lit('?') >> query[bind(&Uri::setQuery, _val, _1)] ) >>
					-( lit('#') >> frag[bind(&Uri::setFragment, _val, _1)] )
				;
			}
	};

	template<typename Iterator>
	void parse_uri(Iterator begin, Iterator end, Uri &uri) {
		auto p = parser<Iterator>(); 

		if(!qi::parse(begin, end, p, uri) || begin != end) {
			COEUS_LOG(Log, Severity::Debug) << "Uri parse failed, Uri: " << static_cast<string>(uri) << " left: " << string(begin, end);

			COEUS_THROW(Exception("Failed to parse Uri"s) <<
				UriInfo(uri)
			);
		}
#		ifdef DEBUG
			COEUS_LOG(Log, Severity::Debug) << "Uri parse succeeded, Uri: " << static_cast<string>(uri) << " left: " << string(begin, end);
#		endif
	}
}

Uri::Uri() :
	_scheme(),
	_path(),
	_authority(),
	_query(),
	_fragment()
{}

Uri::Uri(string const &uri) :
	Uri()
{
	parse_uri(uri.begin(), uri.end(), *this);
}

Uri::operator string () const {
	return
		_scheme + ":"s +
		(_authority ? static_cast<string>(*_authority) : ""s) +
		_path +
		(_query ? "?"s + *_query : ""s) +
		(_fragment ? "#"s + *_fragment : ""s)
	;
}

auto Uri::Scheme() const -> decltype(_scheme) const & {
	return _scheme;
}

auto Uri::Path() const -> decltype(_path) const & {
	return _path;
}

auto Uri::Auth() const -> decltype(*_authority) const & {
	return *_authority;
}

auto Uri::Query() const -> decltype(*_query) const & {
	return *_query;
}

auto Uri::Fragment() const -> decltype(*_fragment) const & {
	return *_fragment;
}

auto Uri::hasAuthority() const -> bool {
	return !!_authority;
}

auto Uri::hasQuery() const -> bool {
	return !!_query;
}

auto Uri::hasFragment() const -> bool {
	return !!_fragment;
}

auto Uri::setScheme(decltype(_scheme) const &v) -> void {
	_scheme = v;
}

auto Uri::setPath(decltype(_path) const &v) -> void {
	_path = v;
}

auto Uri::setAuthority(decltype(*_authority) const &v) -> void {
	_authority = v;
}

auto Uri::setQuery(decltype(*_query) const &v) -> void {
	_query = v;
}

auto Uri::setFragment(decltype(*_fragment) const &v) -> void {
	_fragment = v;
}

Uri::Authority::Authority() :
	_host(),
	_port(),
	_user()
{}

Uri::Authority::operator string () const {
	return "//"s +
		(_user ? *_user + "@"s : ""s) +
		_host +
		(_port ? ":"s + *_port : ""s)
	;
}

auto Uri::Authority::Host() const -> decltype(_host) const & {
	return _host;
}

auto Uri::Authority::Port() const -> decltype(*_port) const & {
	return *_port;
}

auto Uri::Authority::User() const -> decltype(*_user) const & {
	return *_user;
}

auto Uri::Authority::hasPort() const -> bool {
	return !!_port;
}

auto Uri::Authority::hasUser() const -> bool {
	return !!_user;
}

auto Uri::Authority::setHost(decltype(_host) const &v) -> void {
	_host = v;
}

auto Uri::Authority::setPort(decltype(*_port) const &v) -> void {
	_port = v;
}

auto Uri::Authority::setUser(decltype(*_user) const &v) -> void {
	_user = v;
}
