/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Sc2Api.hh"

using namespace Sc2Ai;
using namespace std;

Sc2Api::Sc2Api(
	decltype(declval<Sc2Api>().io()) i,
	decltype(_res) res
) :
	Sc2Ws(i),
	_res(res)
{}

Sc2Api::Sc2Api(
	decltype(declval<Sc2Api>().io()) i,
	decltype(_res) res,
	string const &u
) :
	Sc2Ws(i, u),
	_res(res)
{}

void Sc2Api::onHandshake(error_code const &ec) {
	Sc2Ws::onHandshake(ec);

	_res.onReady();
}

void Sc2Api::onResponse(Response &&r) {
	_res.onResponse(r);
}
