/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "PNG.hh"

using namespace Sc2Ai;
using namespace std;

PNG::PNG() :
	_handle(),
	_pixels()
{
	memset(&_handle, 0, sizeof(_handle));
	_handle.version = PNG_IMAGE_VERSION;
	_handle.format = PNG_FORMAT_GRAY;
}

PNG::PNG(
	uint32_t const w,
	uint32_t const h
) :
	PNG()
{
	resize(w, h);
}

PNG::~PNG()
{
	png_image_free(&_handle);
}

auto PNG::write(string const &name) -> void
{
	png_image_write_to_file(&_handle, name.c_str(), false, _pixels.data(), -PNG_IMAGE_ROW_STRIDE(_handle), nullptr);

	if(_handle.warning_or_error > 0)
	{
		if(_handle.warning_or_error == PNG_IMAGE_WARNING)
			COEUS_LOG(Log, Severity::Warning) << _handle.message;
		else
			COEUS_THROW(
				PNGException(_handle.message) <<
				PNGFileName(name)
			);
	}
}

auto PNG::resize(uint32_t const w, uint32_t const h) -> void {
	// 2020-08-04 AMR TODO: pixel size
	_handle.width = w;
	_handle.height = h;
	_pixels.clear();
	_pixels.resize(w * h);
}

auto PNG::height() const -> uint32_t const &
{
	return _handle.height;
}

auto PNG::width() const -> uint32_t const &
{
	return _handle.width;
}

// 2020-08-04 AMR TODO: stride?
auto PNG::pixel(uint32_t const x, uint32_t const y) -> uint8_t & {
	return _pixels[y * width() + x];
}

auto PNG::pixel(uint32_t const x, uint32_t const y) const -> uint8_t const & {
	return _pixels[y * width() + x];
}
