/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_RAWOBSERVER_INC
#	define SC2AI_RAWOBSERVER_INC

#	include "Observer.hh"

	namespace Sc2Ai
	{
		using ::SC2APIProtocol::StartRaw;
		using ::SC2APIProtocol::PlayerRaw;
		using ::SC2APIProtocol::PowerSource;
		using ::SC2APIProtocol::Unit;
		using ::SC2APIProtocol::Effect;
		using ::SC2APIProtocol::Race;
		using ::SC2APIProtocol::NoRace;

		// 2020-05-10 AMR TODO: cache Upgrade/Ability/Effect Data?
		class RawObserver :
			public Observer
		{
			private:
				Race _race;
			public:
				RawObserver(name_type const &, dep_names_type const &);
				virtual ~RawObserver();
			protected:
				virtual void onGameInfo(ResponseGameInfo const &) override;
				virtual void onObservation(Observation const &) override;
			protected:
				auto race() const -> decltype(_race) const &;
			protected:
				// 2020-05-10 AMR TODO: byref ints?
				virtual void onStart(StartRaw const &) {}
				virtual void onPlayer(PlayerRaw const &);
				virtual void onPowerSource(PowerSource const &) {}
				virtual void onUpgrade(std::uint32_t const &) {}
				virtual void onUnit(Unit const &) {}
				virtual void onDeath(std::uint64_t const &) {}
				virtual void onEffect(Effect const &) {}
				// 2020-05-10 AMR TODO: camera
				// 2020-05-10 AMR TODO: visibility and creep
				// 2020-05-10 AMR TODO: sensor tower visibility (RadarRing?)
		};
	}

#endif
