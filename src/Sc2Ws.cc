/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Sc2Ws.hh"

using namespace Sc2Ai;
using namespace std;

Sc2Ws::Sc2Ws(
	decltype(std::declval<Sc2Ws>().io()) i
) :
	Websocket(i)
{}

Sc2Ws::Sc2Ws(
	decltype(std::declval<Sc2Ws>().io()) i,
	string const &uri
) :
	Websocket(i, uri)
{}

Sc2Ws::~Sc2Ws() = default;

void Sc2Ws::send(Request const &req) {
	auto s = ""s;

	if(!req.SerializeToString(&s)) {
		COEUS_THROW(Exception("Failed to serialize Request"s));
	}

	Websocket::send(move(s));
}

void Sc2Ws::onMessage(string &&m) {
	auto res = Response();

	if(!res.ParseFromString(m)) {
		COEUS_THROW(Exception("Failed to deserialize Response"s));
	}

	onResponse(move(res));
}
