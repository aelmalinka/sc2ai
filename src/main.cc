/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <cstdlib>
#include <iostream>
#include "Application.hh"

using namespace std;
using namespace Sc2Ai;

int main(int argc, char const *argv[]) {
	auto app = make_shared<Application>(argc, argv);

	try {
		(*app)();

		return EXIT_SUCCESS;
	} catch(exception &e) {
		cerr << e.what() << endl;
	}

	return EXIT_FAILURE;
}
