/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Module.hh"

using namespace Sc2Ai;
using namespace std;

Module::Module(
	decltype(_name) const &n,
	decltype(_dep_names) const &d
) :
	_name(n),
	_dep_names(d),
	_vis(bind(&Module::defCall, this, placeholders::_1)),
	Log("Sc2Ai-"s + _name)
{}

Module::~Module() = default;

auto Module::setApi(decltype(_api) const &api) -> void {
	_api = api;
}

auto Module::setApplication(decltype(_app) const &a) -> void {
	_app = a;

	for_each(_dep_names.begin(), _dep_names.end(), bind(&Module::load_dep, this, placeholders::_1));
	app().modules(bind(&Module::save_dep, this, placeholders::_1));

	if(!_dep_names.empty()) {
		// 2020-04-24 AMR TODO: is this what we want?
		COEUS_LOG(Log, Severity::Error) << "Failed to load all dependencies";
	}
}

auto Module::name() const -> decltype(_name) const & {
	return _name;
}

any Module::defCall(any const &p) {
	COEUS_LOG(Log, Severity::Error) << "Unknown call in module to " << detail::demangle(p.type().name());

	return {};
}

void Module::onError(decltype(declval<Response>().error()) const &errors) {
	for_each(errors.begin(), errors.end(), [this](auto const &error) {
		// 2020-04-24 AMR TODO: this might constitute a throw; but when?
		COEUS_LOG(Log, Severity::Error) << error;
	});
}

auto Module::app() -> decltype(*_app) & {
	return *_app;
}

auto Module::api() -> decltype(*_api) & {
	return *_api;
}

auto Module::dep(string const &modname) -> decltype(_deps)::mapped_type & {
	auto i = _deps.find(modname);
	if(i == _deps.end())
		COEUS_THROW(
			Exception("Failed to find dependency "s + modname) <<
			ModuleName(name())
		);
	return i->second;
}

auto Module::load_dep(string const &name) -> void {
	if(!app().load(name))
		COEUS_LOG(Log, Severity::Error) << "Failed to load " << name;
}

auto Module::save_dep(decltype(_deps)::mapped_type const &mod) -> void {
	auto i = find(_dep_names.begin(), _dep_names.end(), mod->name());
	if(i != _dep_names.end()) {
		_deps.emplace(mod->name(), mod);
		_dep_names.erase(i);
	}
}
