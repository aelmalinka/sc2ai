/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_SC2API_INC
#	define SC2AI_SC2API_INC

#	include "Sc2Ws.hh"
#	include "Responder.hh"

	namespace Sc2Ai
	{
		class Sc2Api :
			public Sc2Ws
		{
			private:
				Responder &_res;
			public:
				Sc2Api(
					decltype(std::declval<Sc2Api>().io()),
					decltype(_res)
				);
				Sc2Api(
					decltype(std::declval<Sc2Api>().io()),
					decltype(_res),
					std::string const &
				);
			protected:
				void onHandshake(error_code const &) override;
				void onResponse(Response &&) override;
		};
	}

#endif
