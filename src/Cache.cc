/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Cache.hh"
#include "s2clientprotocol/sc2api.pb.h"

using namespace std;

using ::SC2APIProtocol::Unit;
using ::SC2APIProtocol::PassengerUnit;
using ::SC2APIProtocol::Effect;
using ::SC2APIProtocol::AbilityData;
using ::SC2APIProtocol::UnitTypeData;
using ::SC2APIProtocol::UpgradeData;
using ::SC2APIProtocol::BuffData;
using ::SC2APIProtocol::EffectData;

namespace Sc2Ai { namespace detail {
	template<>
	auto get_id(Unit const &u) -> uint64_t {
		return u.tag();
	}

	template<>
	auto get_id(PassengerUnit const &u) -> uint64_t {
		return u.tag();
	}

	template<>
	auto get_id(Effect const &u) -> uint64_t {
		return u.effect_id();
	}

	template<>
	auto get_id(AbilityData const &u) -> uint64_t {
		return u.ability_id();
	}

	template<>
	auto get_id(UnitTypeData const &u) -> uint64_t {
		return u.unit_id();
	}

	template<>
	auto get_id(UpgradeData const &u) -> uint64_t {
		return u.upgrade_id();
	}

	template<>
	auto get_id(BuffData const &u) -> uint64_t {
		return u.buff_id();
	}

	template<>
	auto get_id(EffectData const &u) -> uint64_t {
		return u.effect_id();
	}
} }
