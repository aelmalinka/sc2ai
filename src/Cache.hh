/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_CACHE_INC
#	define SC2AI_CACHE_INC

#	include <functional>
#	include <initializer_list>
#	include "detail/CacheIterator.hh"

	namespace Sc2Ai
	{
		namespace detail {
			template<typename T, typename K = std::uint64_t>
			auto get_id(T const &) -> K;
		}

		template<typename T, typename K = std::uint64_t>
		class Cache
		{
			private:
				using map_type = std::map<K, T>;
			public:
				using value_type = T;
				using reference = T &;
				using const_reference = T const &;
				using iterator = detail::CacheIterator<T, typename map_type::iterator>;
				using reverse_iterator = detail::CacheIterator<T, typename map_type::reverse_iterator>;
				using const_iterator = detail::CacheIterator<const T, typename map_type::const_iterator>;
				using const_reverse_iterator = detail::CacheIterator<const T, typename map_type::const_reverse_iterator>;
				using difference_type = typename std::iterator_traits<iterator>::difference_type;
				using size_type = std::size_t;
			private:
				map_type _data;
				std::function<bool(T const &)> _filter;
			public:
				Cache();
				explicit Cache(decltype(_filter) const &);
				explicit Cache(std::initializer_list<T> const &);
				Cache(const Cache<T, K> &);
				Cache(Cache<T, K> &&);
				auto filter(decltype(_filter) const &) -> void;
				auto insert(const_reference) -> bool;
				auto insert(value_type &&) -> bool;
				auto remove(K const) -> bool;
				auto clear() -> void;
				auto find(K const &) -> iterator;
				auto find(K const &) const -> const_iterator;
				auto operator [] (K const &) -> reference;
				auto begin() -> iterator;
				auto begin() const -> const_iterator;
				auto end() -> iterator;
				auto end() const -> const_iterator;
				auto cbegin() const -> const_iterator;
				auto cend() const -> const_iterator;
				auto rbegin() -> reverse_iterator;
				auto rbegin() const -> const_reverse_iterator;
				auto rend() -> reverse_iterator;
				auto rend() const -> const_reverse_iterator;
				auto crbegin() const -> const_reverse_iterator;
				auto crend() const -> const_reverse_iterator;
				auto operator == (Cache<T, K> const &) const -> bool;
				auto operator != (Cache<T, K> const &) const -> bool;
				auto swap(Cache<T, K> &) -> void;
				auto size() const -> size_type;
				auto max_size() const -> size_type;
				auto empty() const -> bool;
				auto operator = (Cache<T, K> const &) -> Cache<T, K> &;
				auto operator = (Cache<T, K> &&) -> Cache<T, K> &;
		};
	}

#	include "Cache.impl.hh"

#endif
