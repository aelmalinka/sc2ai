/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_VISITOR_IMPL
#	define SC2AI_VISITOR_IMPL

	namespace Sc2Ai
	{
		namespace detail {
			template<typename ...Args>
			class Caller;

			template<typename ...Args>
			struct Caller {
				Caller(Visitor const &v) :
					_visitor(v)
				{}
				std::any operator () (Args && ...args) const {
					return _visitor.call(std::make_tuple(std::forward<Args>(args)...));
				}
				private:
					Visitor const &_visitor;
			};

			template<>
			struct Caller<> {
				Caller(Visitor const &v) :
					_visitor(v)
				{}
				std::any operator () () const {
					return _visitor.call(std::any{});
				}
				private:
					Visitor const &_visitor;
			};

			template<typename T>
			struct Caller<T> {
				Caller(Visitor const &v) :
					_visitor(v)
				{}
				std::any operator () (T &&t) const {
					if constexpr (std::is_same_v<std::remove_cv_t<std::remove_reference_t<T>>, std::any>) {
						return _visitor.call(std::forward<T>(t));
					} else {
						return _visitor.call(std::any(std::forward<T>(t)));
					}
				}
				private:
					Visitor const &_visitor;
			};

			template<typename ...Args>
			struct CallF;

			template<typename F, typename ...Args>
			struct CallF<
				F,
				Args...
			> {
				CallF(F f) :
					_f(f)
				{
					static_assert(sizeof...(Args) > 1);
				}
				auto operator () (std::any const &a) const -> std::any {
					if constexpr (std::is_same_v<std::invoke_result_t<F, Args...>, void>) {
						std::apply(_f, std::any_cast<std::tuple<Args...>>(a));
						return {};
					} else {
						return std::apply(_f, std::any_cast<std::tuple<Args...>>(a));
					}
				}
				private:
					F _f;
			};

			template<typename F, typename T>
			struct CallF<
				F,
				T
			> {
				CallF(F f) :
					_f(f)
				{
					static_assert(!std::is_same_v<T, void>);
				}
				auto operator () (std::any const &a) const -> std::any {
					if constexpr (std::is_same_v<std::invoke_result_t<F, T>, void>) {
						std::invoke(_f, std::any_cast<T>(a));
						return {};
					} else {
						return std::invoke(_f, std::any_cast<T>(a));
					}
				}
				private:
					F _f;
			};

			template<typename F>
			struct CallF<
				F,
				void
			> {
				CallF(F f) :
					_f(f)
				{}
				auto operator () (std::any const &) const -> std::any {
					if constexpr (std::is_same_v<std::invoke_result_t<F>, void>) {
						_f();
						return {};
					} else {
						return _f();
					}
				}
				private:
					F _f;
			};
		}

		template<typename ...Args, typename F>
		auto Visitor::add(F const &f) -> void {
			auto const &k = get_key<Args...>();

			COEUS_LOG(Sc2Ai::Log, Coeus::Severity::Debug) << "Adding " << detail::demangle(k.name());

			_visits.emplace(
				k,
				get_f<Args...>(f)
			);
		}

		template<typename ...Args>
		auto Visitor::operator () (Args && ...args) const -> std::any {
			return detail::Caller<Args...>(*this)(std::forward<Args>(args)...);
		}

		template<typename ...Args>
		auto Visitor::get_key() -> key_type {
			if constexpr (sizeof...(Args) > 1)
				return typeid(typename std::tuple<Args...>);
			else if constexpr (sizeof...(Args) == 1)
				return typeid(typename std::remove_cv<Args...>::type);
			else
				return typeid(void);
		}

		template<typename ...Args, typename F>
		auto Visitor::get_f(F const &f) -> f_type {
			return detail::CallF<F, Args...>(f);
		}
	}

#endif
