/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_MODULE_IMPL
#	define SC2AI_MODULE_IMPL

	namespace Sc2Ai
	{
		template<typename ...Args>
		auto Module::call(Args && ...args) const -> std::any {
			return _vis(std::forward<Args>(args)...);
		}

		template<typename ...Args, typename F>
		auto Module::add(F &&f) -> void {
			_vis.add<Args...>(std::forward<F>(f));
		}
	}

#endif
