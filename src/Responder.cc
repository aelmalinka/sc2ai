/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Responder.hh"

using namespace Sc2Ai;
using namespace std;

Responder::Responder() :
	_state(SC2APIProtocol::unknown)
{}

Responder::~Responder() = default;

auto Responder::state() const -> decltype(_state) const & {
	return _state;
}

void Responder::onResponse(Response const &res) {
	if(res.status() != _state)
		onStateChange(_state = res.status());

	switch(res.response_case()) {
		case Response::kCreateGame:
			onCreateGame(res.create_game());
			break;
		case Response::kJoinGame:
			onJoinGame(res.join_game());
			break;
		case Response::kRestartGame:
			onRestartGame(res.restart_game());
			break;
		case Response::kStartReplay:
			onStartReplay(res.start_replay());
			break;
		case Response::kLeaveGame:
			onLeaveGame(res.leave_game());
			break;
		case Response::kQuickSave:
			onQuickSave(res.quick_save());
			break;
		case Response::kQuickLoad:
			onQuickLoad(res.quick_load());
			break;
		case Response::kQuit:
			onQuit(res.quit());
			break;
		case Response::kGameInfo:
			onGameInfo(res.game_info());
			break;
		case Response::kObservation:
			onObservation(res.observation());
			break;
		case Response::kAction:
			onAction(res.action());
			break;
		case Response::kObsAction:
			onObserverAction(res.obs_action());
			break;
		case Response::kStep:
			onStep(res.step());
			break;
		case Response::kData:
			onData(res.data());
			break;
		case Response::kQuery:
			onQuery(res.query());
			break;
		case Response::kSaveReplay:
			onSaveReplay(res.save_replay());
			break;
		case Response::kReplayInfo:
			onReplayInfo(res.replay_info());
			break;
		case Response::kAvailableMaps:
			onAvailableMaps(res.available_maps());
			break;
		case Response::kSaveMap:
			onSaveMap(res.save_map());
			break;
		case Response::kMapCommand:
			onMapCommand(res.map_command());
			break;
		case Response::kPing:
			onPing(res.ping());
			break;
		case Response::kDebug:
			onDebug(res.debug());
			break;
		case Response::RESPONSE_NOT_SET:
			return onError(res.error());
	}

	// 2020-04-21 AMR NOTE: if response_case is set error holds warnings
	if(res.error_size() != 0) {
		for(auto &e : res.error()) {
			COEUS_LOG(Log, Severity::Warning) << e;
		}
	}
}
