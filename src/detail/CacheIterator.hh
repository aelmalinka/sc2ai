/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_DETAIL_CACHE_INC
#	define SC2AI_DETAIL_CACHE_INC

#	include <map>
#	include <iterator>

	namespace Sc2Ai
	{
		namespace detail
		{
			template<
				typename T,
				typename base_iterator = typename std::map<std::uint64_t, T>::iterator
			>
			class CacheIterator
			{
				public:
					using self = CacheIterator<T, base_iterator>;
					using iterator_category = std::bidirectional_iterator_tag;
					using value_type = T;
					using difference_type = std::ptrdiff_t;
					using pointer = T *;
					using reference = T &;
				private:
					base_iterator _current;
				public:
					explicit CacheIterator(base_iterator const & = {});
					auto operator == (self const &) const -> bool;
					auto operator != (self const &) const -> bool;
					auto operator * () -> reference;
					auto operator ->() -> pointer;
					auto operator ++ () -> self &;
					auto operator -- () -> self &;
					auto operator ++ (int) -> self;
					auto operator -- (int) -> self;
			};
		}
	}

#	include "CacheIterator.impl.hh"

#endif
