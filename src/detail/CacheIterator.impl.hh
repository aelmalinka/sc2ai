/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_DETAIL_CACHEITERATOR_IMPL
#	define SC2AI_DETAIL_CACHEITERATOR_IMPL

	namespace Sc2Ai
	{
		namespace detail
		{
			template<typename T, typename base_iterator>
			CacheIterator<T, base_iterator>::CacheIterator(base_iterator const &i) :
				_current(i)
			{}

			template<typename T, typename base_iterator>
			auto CacheIterator<T, base_iterator>::operator == (self const &o) const -> bool {
				return _current == o._current;
			}

			template<typename T, typename base_iterator>
			auto CacheIterator<T, base_iterator>::operator != (self const &o) const -> bool {
				return _current != o._current;
			}

			template<typename T, typename base_iterator>
			auto CacheIterator<T, base_iterator>::operator * () -> reference {
				return _current->second;
			}

			template<typename T, typename base_iterator>
			auto CacheIterator<T, base_iterator>::operator -> () -> pointer {
				return &_current->second;
			}

			template<typename T, typename base_iterator>
			auto CacheIterator<T, base_iterator>::operator ++ () -> self & {
				++_current;

				return *this;
			}

			template<typename T, typename base_iterator>
			auto CacheIterator<T, base_iterator>::operator -- () -> self & {
				--_current;

				return *this;
			}

			template<typename T, typename base_iterator>
			auto CacheIterator<T, base_iterator>::operator ++ (int) -> self {
				auto ret = self(_current);

				++_current;

				return ret;
			}

			template<typename T, typename base_iterator>
			auto CacheIterator<T, base_iterator>::operator -- (int) -> self {
				auto ret = self(_current);

				--_current;

				return ret;
			}
		}
	}

#endif
