/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_VISITOR_INC
#	define SC2AI_VISITOR_INC

#	include <any>
#	include <functional>
#	include <initializer_list>
#	include <type_traits>
#	include <typeindex>
#	include <typeinfo>
#	include <unordered_map>
#	include "Exception.hh"

	namespace Sc2Ai
	{
		namespace detail {
			std::string demangle(const char *);
		}

		COEUS_ERROR_INFO(AnyInfo, std::any);
		COEUS_ERROR_INFO(TypeInfo, std::type_index);

		// 2020-06-21 AMR TODO: detect param types and remove Args req
		class Visitor
		{
			private:
				using key_type = std::type_index;
				using f_type = std::function<std::any(std::any const &)>;
				using container_type = std::unordered_map<key_type, f_type>;
				container_type _visits;
				f_type _def;
			public:
				Visitor();
				explicit Visitor(f_type const &);
				auto setDefault(f_type const &) -> void;
				auto clearDefault() -> void;
				template<typename ...Args, typename F>
				auto add(F const &) -> void;
				template<typename ...Args>
				auto operator () (Args && ...) const -> std::any;
				auto call(std::any const &) const -> std::any;
			private:
				template<typename ...Args>
				static auto get_key() -> key_type;
				template<typename ...Args, typename F>
				static auto get_f(F const &) -> f_type;
		};
	}

#	include "Visitor.impl.hh"

#endif
