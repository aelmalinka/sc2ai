/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "Application.hh"

using namespace Sc2Ai;
using namespace std;

using Coeus::value;
using Coeus::Import;

Application::Application(
	int const ArgC,
	char const *ArgV[]
) :
	::Coeus::Application(ArgC, ArgV)
{
	_opts.add_options()
		("url,u", value(&_uri)->default_value(SC2AI_DEFAULT_URI), "The url of the Websocket interface to SC2")
		("import-path,i", value(&_search)->composing(), "The path(s) to search for modules for import")
		("module,m", value(&_mod_names)->composing(), "The module(s) to load on start")
	;

	command_line().add(_opts);
	config_file().add(_opts);
}

Application::~Application() = default;

void Application::operator () () {
	::Coeus::Application::operator () ();

	COEUS_LOG(Log, Severity::Debug) << "Url: " << _uri;

	_api = make_shared<Sc2Api>(_ctx, *this, _uri);
	for_each(_search.begin(), _search.end(), Import<Module>::addSearchPath);
	for_each(_mod_names.begin(), _mod_names.end(), bind(&Application::load, this, placeholders::_1));

	_ctx.run();
}

bool Application::load(string const &name) {
	if(modules().find(name) == modules().end()) {
		COEUS_LOG(Log, Severity::Info) << "Loading " << name;
		auto i = modules().emplace(name, name);

		if(!i.second)
			return false;
		if(!i.first->second) {
			_modules.erase(i.first);
			return false;
		} else {
			auto mod = i.first->second;
			mod->setApi(_api);
			mod->setApplication(shared_from_this());
			mod->onLoaded();

			return true;
		}
	} else {
		return true;
	}
}

auto Application::unload(string const &name) -> decltype(_modules.erase(string{})) {
	return modules().erase(name);
}

void Application::modules(function<void(decltype(_modules)::mapped_type &)> const &f) {
	if(f)
		for_each(
			modules().begin(),
			modules().end(),
			bind(f, bind(&decltype(_modules)::value_type::second, placeholders::_1))
		);
}

auto Application::data() -> decltype(_data) * {
	return &_data;
}

void Application::onResponse(Response const &r) {
	modules(bind(&Responder::onResponse, placeholders::_1, cref(r)));

	Responder::onResponse(r);
}

void Application::onReady() {
	modules(bind(&Responder::onReady, placeholders::_1));
}

void Application::onGameInfo(ResponseGameInfo const &r) {
	Responder::onGameInfo(r);

	auto req = Request{};
	auto data = req.mutable_data();

	_data.requestData(data);

	_api->send(move(req));
}

void Application::onError(decltype(declval<Response>().error()) const &) {
	COEUS_THROW(Exception("Application::onError called directly"s));
}

decltype(Application::_modules) &Application::modules() {
	return _modules;
}

Application::Data::Data() :
	_abilities(false),
	_unit_types(false),
	_upgrades(false),
	_buffs(false),
	_effects(false)
{}

void Application::Data::requestData(RequestData *req) const {
	if(_abilities) req->set_ability_id(true);
	if(_unit_types) req->set_unit_type_id(true);
	if(_upgrades) req->set_upgrade_id(true);
	if(_buffs) req->set_buff_id(true);
	if(_effects) req->set_effect_id(true);
}

void Application::Data::Abilities() {
	_abilities = true;
}

void Application::Data::UnitTypes() {
	_unit_types = true;
}

void Application::Data::Upgrades() {
	_upgrades = true;
}

void Application::Data::Buffs() {
	_buffs = true;
}

void Application::Data::Effects() {
	_effects = true;
}
