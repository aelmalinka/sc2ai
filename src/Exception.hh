/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_EXCEPTION_INC
#	define SC2AI_EXCEPTION_INC

#	include <Coeus/Exception.hh>
#	include <Coeus/Log.hh>

	namespace Sc2Ai
	{
		using Coeus::Log::Severity;
		extern Coeus::Log::Source Log;
		COEUS_EXCEPTION(Exception, "Sc2Ai Exception", Coeus::Exception);
	}

#endif
