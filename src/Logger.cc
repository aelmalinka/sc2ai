/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Exception.hh"

using namespace std;
using namespace Coeus::Log;

namespace Sc2Ai {
	Source Log("Sc2Ai"s);
}
