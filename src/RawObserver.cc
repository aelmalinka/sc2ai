/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "RawObserver.hh"

using namespace Sc2Ai;
using namespace std;

RawObserver::RawObserver(name_type const &n, dep_names_type const &d) :
	Observer(n, d),
	_race(NoRace)
{}

RawObserver::~RawObserver() = default;

auto RawObserver::race() const -> decltype(_race) const & {
	return _race;
}

void RawObserver::onGameInfo(ResponseGameInfo const &res) {
	for(auto &i : res.player_info()) {
		if(
			i.has_player_id() &&
			i.player_id() == id() &&
			i.has_race_actual()
		)
			_race = i.race_actual();
	}

	if(_race == NoRace)
		COEUS_THROW(Exception("Unkonwn Race or I'm not a player"s));
	if(!res.has_start_raw())
		COEUS_THROW(Exception("Game not in Raw mode"s));

	Observer::onGameInfo(res);
	onStart(res.start_raw());
}

void RawObserver::onObservation(Observation const &obs) {
	if(!obs.has_raw_data())
		COEUS_THROW(Exception("Game not in Raw mode"s));

	auto raw = obs.raw_data();

	if(raw.has_player()) {
		onPlayer(raw.player());
	}

	for(auto &i : raw.units()) {
		onUnit(i);
	}

	if(raw.has_event()) {
		for(auto &i : raw.event().dead_units()) {
			onDeath(i);
		}
	}

	for(auto &i : raw.effects()) {
		onEffect(i);
	}

	Observer::onObservation(obs);
}

void RawObserver::onPlayer(PlayerRaw const &player) {
	for(auto &i : player.power_sources()) {
		onPowerSource(i);
	}
	for(auto &i : player.upgrade_ids()) {
		onUpgrade(i);
	}
}
