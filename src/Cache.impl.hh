/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_CACHE_IMPL
#	define SC2AI_CACHE_IMPL

#	include <google/protobuf/util/message_differencer.h>

	namespace Sc2Ai
	{
		template<typename T, typename K>
		Cache<T, K>::Cache() = default;

		template<typename T, typename K>
		Cache<T, K>::Cache(Cache<T, K> const &) = default;
		template<typename T, typename K>
		Cache<T, K>::Cache(Cache<T, K> &&) = default;

		template<typename T, typename K>
		auto Cache<T, K>::operator = (Cache<T, K> const &) -> Cache<T, K> & = default;
		template<typename T, typename K>
		auto Cache<T, K>::operator = (Cache<T, K> &&) -> Cache<T, K> & = default;

		template<typename T, typename K>
		Cache<T, K>::Cache(decltype(_filter) const &f) :
			_data(),
			_filter(f)
		{}

		template<typename T, typename K>
		Cache<T, K>::Cache(std::initializer_list<T> const &il) :
			Cache<T, K>::Cache()
		{
			for(auto &i : il) {
				// 2020-05-17 AMR NOTE: order matters; don't combine
				auto id = detail::get_id(i);
				_data.emplace(id, std::move(i));
			}
		}

		template<typename T, typename K>
		auto Cache<T, K>::filter(decltype(_filter) const &f) -> void {
			_filter = f;
		}

		template<typename T, typename K>
		auto Cache<T, K>::insert(const_reference v) -> bool {
			if(!_filter || _filter(v)) {
				auto id = detail::get_id(v);

				if(_data.find(id) == _data.end()) {
					_data.emplace(id, v);
					return true;
				} else {
					_data[id] = v;
				}
			}

			return false;
		}

		template<typename T, typename K>
		auto Cache<T, K>::insert(value_type &&v) -> bool {
			if(!_filter || _filter(v)) {
				auto id = detail::get_id(v);

				if(_data.find(id) == _data.end()) {
					_data.emplace(id, std::move(v));
					return true;
				} else {
					_data[id] = std::move(v);
				}
			}

			return false;
		}

		template<typename T, typename K>
		auto Cache<T, K>::remove(K const id) -> bool {
			if(_data.find(id) != _data.end()) {
				_data.erase(id);
				return true;
			}

			return false;
		}

		template<typename T, typename K>
		auto Cache<T, K>::clear() -> void {
			_data.clear();
		}

		template<typename T, typename K>
		auto Cache<T, K>::find(K const &k) -> iterator {
			return iterator(_data.find(k));
		}

		template<typename T, typename K>
		auto Cache<T, K>::find(K const &k) const -> const_iterator {
			return const_iterator(_data.find(k));
		}

		template<typename T, typename K>
		auto Cache<T, K>::operator [] (K const &k) -> reference {
			return _data[k];
		}

		template<typename T, typename K>
		auto Cache<T, K>::begin() -> iterator {
			return iterator(_data.begin());
		}

		template<typename T, typename K>
		auto Cache<T, K>::begin() const -> const_iterator {
			return const_iterator(_data.cbegin());
		}
		
		template<typename T, typename K>
		auto Cache<T, K>::end() -> iterator {
			return iterator(_data.end());
		}
		
		template<typename T, typename K>
		auto Cache<T, K>::end() const -> const_iterator {
			return const_iterator(_data.cend());
		}
		
		template<typename T, typename K>
		auto Cache<T, K>::cbegin() const -> const_iterator {
			return const_iterator(_data.cbegin());
		}
		
		template<typename T, typename K>
		auto Cache<T, K>::cend() const -> const_iterator {
			return const_iterator(_data.cend());
		}
		
		template<typename T, typename K>
		auto Cache<T, K>::rbegin() -> reverse_iterator {
			return reverse_iterator(_data.rbegin());
		}
		
		template<typename T, typename K>
		auto Cache<T, K>::rbegin() const -> const_reverse_iterator {
			return const_reverse_iterator(_data.crbegin());
		}
		
		template<typename T, typename K>
		auto Cache<T, K>::rend() -> reverse_iterator {
			return reverse_iterator(_data.rend());
		}
		
		template<typename T, typename K>
		auto Cache<T, K>::rend() const -> const_reverse_iterator {
			return reverse_iterator(_data.crend());
		}
		
		template<typename T, typename K>
		auto Cache<T, K>::crbegin() const -> const_reverse_iterator {
			return const_reverse_iterator(_data.crbegin());
		}
		
		template<typename T, typename K>
		auto Cache<T, K>::crend() const -> const_reverse_iterator {
			return const_reverse_iterator(_data.crend());
		}

		template<typename T, typename K>
		auto Cache<T, K>::operator == (Cache<T, K> const &o) const -> bool {
			if(size() != o.size())
				return false;

			auto i = begin();
			auto j = o.begin();

			while(i != end() && j != o.end()) {
				if(!google::protobuf::util::MessageDifferencer::Equals(*i, *j)) {
					return false;
				}

				++i; ++j;
			}

			return true;
		}

		template<typename T, typename K>
		auto Cache<T, K>::operator != (Cache<T, K> const &o) const -> bool {
			return !(*this == o);
		}

		template<typename T, typename K>
		auto Cache<T, K>::swap(Cache<T, K> &o) -> void {
			std::swap(_data, o._data);
			std::swap(_filter, o._filter);
		}

		template<typename T, typename K>
		auto Cache<T, K>::size() const -> size_type {
			return _data.size();
		}

		template<typename T, typename K>
		auto Cache<T, K>::max_size() const -> size_type {
			return _data.max_size();
		}

		template<typename T, typename K>
		auto Cache<T, K>::empty() const -> bool {
			return _data.empty();
		}
	}

#endif
