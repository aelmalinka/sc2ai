/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_PNG_INC
#	define SC2AI_PNG_INC

#	include <vector>
#	include <cstdint>
#	include <png.h>
#	include "Exception.hh"

	namespace Sc2Ai
	{
		COEUS_EXCEPTION(PNGException, "PNG Exception", Exception);
		COEUS_ERROR_INFO(PNGFileName, std::string);

		class PNG
		{
			private:
				png_image _handle;
				// 2020-08-04 AMR TODO: multiple color channels?
				std::vector<std::uint8_t> _pixels;
			public:
				// 2020-08-04 AMR TODO: read in file
				// 2020-08-04 AMR TODO: formats
				PNG();
				PNG(std::uint32_t const, uint32_t const);
				~PNG();
				// 2020-08-04 AMR TODO: stream based operations?
				auto write(std::string const &) -> void;
				auto resize(std::uint32_t const, std::uint32_t const) -> void;
				auto height() const -> std::uint32_t const &;
				auto width() const -> std::uint32_t const &;
				// 2020-08-04 AMR TODO: operator [] accessors?
				auto pixel(std::uint32_t const, std::uint32_t const) -> std::uint8_t &;
				auto pixel(std::uint32_t const, std::uint32_t const) const -> std::uint8_t const &;
		};
	}

#endif
