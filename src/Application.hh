/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_APPLICATION_INC
#	define SC2AI_APPLICATION_INC

#	include "Module.hh"
#	include <Coeus/Application.hh>

	namespace Sc2Ai
	{
		using ::SC2APIProtocol::RequestData;

		class Application :
			public Coeus::Application,
			public Responder,
			public std::enable_shared_from_this<Application>
		{
			private:
				Coeus::options_description _opts;
				std::string _uri;
				std::vector<std::string> _search;
				std::vector<std::string> _mod_names;
			protected:
				class Data {
					public:
						Data();
						void Abilities();
						void UnitTypes();
						void Upgrades();
						void Buffs();
						void Effects();
						void requestData(RequestData *) const;
					private:
						bool _abilities;
						bool _unit_types;
						bool _upgrades;
						bool _buffs;
						bool _effects;
				};
			private:
				net::io_context _ctx;
				std::shared_ptr<Sc2Api> _api;
				std::map<std::string, Coeus::Import<Module>> _modules;
				Data _data;
			public:
				Application(int const ArgC, char const *ArgV[]);
				virtual ~Application();
				virtual void operator () () override;
				virtual bool load(std::string const &);
				virtual void modules(std::function<void(decltype(_modules)::mapped_type &)> const &);
				auto unload(std::string const &) -> decltype(_modules.erase(std::string{}));
				auto data() -> decltype(_data) *;
			protected:
				void onResponse(Response const &) final;
				void onReady() final;
				void onError(decltype(std::declval<Response>().error()) const &) final;
				void onGameInfo(ResponseGameInfo const &) final;
			protected:
				virtual decltype(_modules) &modules();
		};
	}

#endif
