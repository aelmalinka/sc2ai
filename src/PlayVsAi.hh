/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_PLAYVSAI_INC
#	define SC2AI_PLAYVSAI_INC

#	include "Observer.hh"

	namespace Sc2Ai
	{
		using ::SC2APIProtocol::RequestCreateGame;
		using ::SC2APIProtocol::RequestJoinGame;

		class PlayVsAi :
			public Observer
		{
			public:
				explicit PlayVsAi(name_type const &, dep_names_type const &);
				virtual ~PlayVsAi();
			protected:
				virtual void onReady() override;
				virtual void onCreateGame(ResponseCreateGame const &) override;
				virtual void onJoinGame(ResponseJoinGame const &) override;
				virtual void onGameInfo(ResponseGameInfo const &) override;
				virtual void onObservation(ResponseObservation const &) override;
				virtual void onStep(ResponseStep const &) override;
			protected:
				virtual void onDone();
			protected:
				virtual void requestCreateGame(RequestCreateGame *) = 0;
				virtual void requestJoinGame(ResponseCreateGame const &, RequestJoinGame *) = 0;
		};
	}

#endif
