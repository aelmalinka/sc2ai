/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Observer.hh"

using namespace Sc2Ai;
using namespace std;

using ::SC2APIProtocol::in_game;

Observer::Observer(
	name_type const &name,
	dep_names_type const &deps
) :
	Module(name, deps),
	_id(-1),
	_ready(false),
	_obs()
{}

Observer::~Observer() = default;

auto Observer::id() const -> decltype(_id) const & {
	return _id;
}

auto Observer::doAction(vector<Action> &&a) -> void {
	// 2020-05-28 AMR NOTE: ignoring misbehaving modules that queue actions outside of game
	if(state() == in_game) {
		auto req = Request{};
		auto act = req.mutable_action();

		for(auto &i : a) {
			*act->add_actions() = move(i);
		}

		api().send(move(req));
	} else
		COEUS_LOG(Log, Severity::Error) << "Queueing action outside of gameplay";
}

void Observer::onJoinGame(ResponseJoinGame const &res) {
	Module::onJoinGame(res);

	if(!res.has_player_id())
		COEUS_LOG(Log, Severity::Error) << "Joined game, no id";
	else
		_id = res.player_id();
}

void Observer::onObservation(ResponseObservation const &res) {
	if(!_ready) {
		_obs.push(res);
		return;
	}

	Module::onObservation(res);

	for(auto &i : res.actions()) {
		onAction(i);
	}

	for(auto &i : res.action_errors()) {
		onActionError(i);
	}

	if(res.has_observation())
		onObservation(res.observation());

	for(auto &i : res.player_result()) {
		onPlayerResult(i);
	}

	for(auto &i : res.chat()) {
		onChatReceived(i);
	}
}

void Observer::onData(ResponseData const &res) {
	Module::onData(res);

	for(auto &i : res.abilities())
		onAbilityData(i);
	for(auto &i : res.units())
		onUnitTypeData(i);
	for(auto &i : res.upgrades())
		onUpgradeData(i);
	for(auto &i : res.buffs())
		onBuffData(i);
	for(auto &i : res.effects())
		onEffectData(i);

	_ready = true;
	while(!_obs.empty()) {
		onObservation(_obs.front());
		_obs.pop();
	}
}

void Observer::onObservation(Observation const &obs) {
	if(obs.has_player_common())
		onPlayer(obs.player_common());
}
