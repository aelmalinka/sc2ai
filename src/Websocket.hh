/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_WEBSOCKET_INC
#	define SC2AI_WEBSOCKET_INC

#	include "Exception.hh"
#	include <queue>
#	include <list>
#	include <boost/asio/io_context.hpp>
#	include <boost/beast/core.hpp>
#	include <boost/beast/websocket.hpp>

	namespace Sc2Ai
	{
		namespace net = boost::asio;
		namespace beast = boost::beast;
		namespace websocket = beast::websocket;

		using tcp = net::ip::tcp;
		using error_code = boost::system::error_code;

		class Websocket {
			protected:
				using buffer = beast::flat_buffer;
			private:
				net::io_context &_io;
				websocket::stream<beast::tcp_stream> _sock;
				bool _connected;
				bool _sending;
				std::string _host;
				std::string _path;
				std::queue<std::string> _reqs;
				std::list<buffer> _buffs;
			public:
				explicit Websocket(decltype(_io));
				Websocket(decltype(_io), std::string const &);
				virtual ~Websocket();
				auto connect(std::string const &) -> void;
				auto close() -> void;
				auto send(std::string &&) -> void;
			protected:
				virtual void onResolve(error_code const &, tcp::resolver::results_type const &);
				virtual void onConnect(error_code const &, tcp::resolver::endpoint_type const &);
				virtual void onHandshake(error_code const &);
				virtual void onRead(error_code const &, std::string &&);
				virtual void onWrite(error_code const &, std::size_t const);
				virtual void onShutdown(error_code const &);
				virtual void onMessage(std::string &&) = 0;
			protected:
				auto io() -> decltype(_io);
			private:
				auto start_read() -> void;
				auto start_send() -> void;
				auto send_req(decltype(_reqs.front()) const &) -> void;
		};
	}

#endif
