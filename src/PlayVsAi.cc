/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "PlayVsAi.hh"

using namespace Sc2Ai;
using namespace std;

using ::SC2APIProtocol::in_game;
using ::SC2APIProtocol::ended;

PlayVsAi::PlayVsAi(name_type const &name, dep_names_type const &deps) :
	Observer(name, deps)
{}

PlayVsAi::~PlayVsAi() = default;

void PlayVsAi::onReady() {
	auto req = Request{};

	requestCreateGame(req.mutable_create_game());

	api().send(move(req));
}

void PlayVsAi::onCreateGame(ResponseCreateGame const &res) {
	if(res.has_error() || res.has_error_details()) {
		COEUS_LOG(Log, Severity::Error) << "Error creating res: " <<
			(res.has_error() ? ResponseCreateGame_Error_Name(res.error()) + " "s : "") <<
			(res.has_error_details() ? "("s + res.error_details() + ")"s : "")
		;
	} else {
		COEUS_LOG(Log, Severity::Info) << "Game created";

		auto req = Request{};

		requestJoinGame(res, req.mutable_join_game());

		api().send(move(req));
	}
}

void PlayVsAi::onJoinGame(ResponseJoinGame const &res) {
	if(res.has_error() || res.has_error_details()) {
		COEUS_LOG(Log, Severity::Error) << "Error joining game: " <<
			(res.has_error() ? ResponseJoinGame_Error_Name(res.error()) + " "s : "") <<
			(res.has_error_details() ? "("s + res.error_details() + ")"s : "")
		;
	} else {
		COEUS_LOG(Log, Severity::Info) << "Game joined";

		auto req = Request{};
		req.mutable_game_info();

		api().send(move(req));
	}
}

void PlayVsAi::onGameInfo(ResponseGameInfo const &) {
	COEUS_LOG(Log, Severity::Debug) << "GameInfo";

	auto req = Request{};

	req.mutable_observation();

	api().send(move(req));
}

void PlayVsAi::onObservation(ResponseObservation const &res) {
	if(state() == in_game) {
		auto req = Request{};

		req.mutable_step();

		api().send(move(req));
	} else if(state() == ended) {
		// 2020-05-28 AMR TODO: this happens more than once; seems wrong
		onDone();
	} else {
		COEUS_LOG(Log, Severity::Error) << "Unexpected status (" << Status_Name(state()) << "); game will likely not continue";
	}

	Observer::onObservation(res);
}

void PlayVsAi::onStep(ResponseStep const &) {
	auto req = Request{};
	req.mutable_observation();

	api().send(move(req));
}

void PlayVsAi::onDone() {
	COEUS_LOG(Log, Severity::Info) << "Game finished; shutting down";
	auto req = Request{};
	req.mutable_quit();

	api().send(move(req));
}
