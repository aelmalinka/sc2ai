/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_RESPONDER_INC
#	define SC2AI_RESPONDER_INC

#	include "Exception.hh"
#	include "s2clientprotocol/sc2api.pb.h"

	namespace Sc2Ai
	{
		using ::SC2APIProtocol::Response;
		using ::SC2APIProtocol::ResponseCreateGame;
		using ::SC2APIProtocol::ResponseJoinGame;
		using ::SC2APIProtocol::ResponseRestartGame;
		using ::SC2APIProtocol::ResponseStartReplay;
		using ::SC2APIProtocol::ResponseLeaveGame;
		using ::SC2APIProtocol::ResponseQuickSave;
		using ::SC2APIProtocol::ResponseQuickLoad;
		using ::SC2APIProtocol::ResponseQuit;
		using ::SC2APIProtocol::ResponseGameInfo;
		using ::SC2APIProtocol::ResponseObservation;
		using ::SC2APIProtocol::ResponseAction;
		using ::SC2APIProtocol::ResponseObserverAction;
		using ::SC2APIProtocol::ResponseStep;
		using ::SC2APIProtocol::ResponseData;
		using ::SC2APIProtocol::ResponseQuery;
		using ::SC2APIProtocol::ResponseSaveReplay;
		using ::SC2APIProtocol::ResponseReplayInfo;
		using ::SC2APIProtocol::ResponseAvailableMaps;
		using ::SC2APIProtocol::ResponseSaveMap;
		using ::SC2APIProtocol::ResponseMapCommand;
		using ::SC2APIProtocol::ResponsePing;
		using ::SC2APIProtocol::ResponseDebug;
		using ::SC2APIProtocol::Status;

		class Responder {
			private:
				Status _state;
			public:
				Responder();
				virtual ~Responder();
				virtual void onResponse(Response const &);
				virtual void onReady() {}
			protected:
				auto state() const -> decltype(_state) const &;
			protected:
				virtual void onError(decltype(std::declval<Response>().error()) const &) = 0;
				virtual void onStateChange(Status const &)  {}
				virtual void onCreateGame(ResponseCreateGame const &) {}
				virtual void onJoinGame(ResponseJoinGame const &) {}
				virtual void onRestartGame(ResponseRestartGame const &) {}
				virtual void onStartReplay(ResponseStartReplay const &) {}
				virtual void onLeaveGame(ResponseLeaveGame const &) {}
				virtual void onQuickSave(ResponseQuickSave const &) {}
				virtual void onQuickLoad(ResponseQuickLoad const &) {}
				virtual void onQuit(ResponseQuit const &) {}
				virtual void onGameInfo(ResponseGameInfo const &) {}
				virtual void onObservation(ResponseObservation const &) {}
				virtual void onAction(ResponseAction const &) {}
				virtual void onObserverAction(ResponseObserverAction const &) {}
				virtual void onStep(ResponseStep const &) {}
				virtual void onData(ResponseData const &) {}
				virtual void onQuery(ResponseQuery const &) {}
				virtual void onSaveReplay(ResponseSaveReplay const &) {}
				virtual void onReplayInfo(ResponseReplayInfo const &) {}
				virtual void onAvailableMaps(ResponseAvailableMaps const &) {}
				virtual void onSaveMap(ResponseSaveMap const &) {}
				virtual void onMapCommand(ResponseMapCommand const &) {}
				virtual void onPing(ResponsePing const &) {}
				virtual void onDebug(ResponseDebug const &) {}
		};
	}

#endif
