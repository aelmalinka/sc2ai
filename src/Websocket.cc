/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Websocket.hh"
#include "Uri.hh"

using namespace Sc2Ai;
using namespace std;

Websocket::Websocket(
	decltype(_io) io
) :
	_io(io),
	_sock(_io),
	_connected(false),
	_sending(false),
	_host(),
	_path()
{}

Websocket::Websocket(
	decltype(_io) io,
	string const &u
) :
	Websocket(io)
{
	connect(u);
}

auto Websocket::connect(string const &u) -> void {
	auto uri = Uri(u);
	auto r = make_shared<tcp::resolver>(_io);

	if(!uri.hasAuthority())
		COEUS_THROW(Exception("websocket uri requires "s));

	_host = uri.Auth().Host();
	_path = uri.Path();

	r->async_resolve(_host, (uri.Auth().hasPort() ? uri.Auth().Port() : uri.Scheme()), [this, r](const auto &a, const auto &b) {
		this->onResolve(a, b);
	});
}

Websocket::~Websocket() {
	close();
}

auto Websocket::close() -> void {
}

auto Websocket::send(string &&req) -> void {
	if(_connected && !_sending) {
		send_req(req);
	} else {
		_reqs.emplace(move(req));
	}
}

void Websocket::onResolve(error_code const &ec, tcp::resolver::results_type const &res) {
	if(ec)
		COEUS_THROW(system_error{ec});

	get_lowest_layer(_sock).async_connect(res, bind(&Websocket::onConnect, this, placeholders::_1, placeholders::_2));
}

void Websocket::onConnect(error_code const &ec, tcp::resolver::endpoint_type const &) {
	if(ec)
		COEUS_THROW(system_error{ec});

	beast::get_lowest_layer(_sock).expires_never();
	_sock.set_option(
		websocket::stream_base::timeout::suggested(
			beast::role_type::client
		)
	);

	_sock.async_handshake(_host, _path, bind(&Websocket::onHandshake, this, placeholders::_1));
}

void Websocket::onHandshake(error_code const &ec) {
	if(ec)
		COEUS_THROW(system_error{ec});

	_connected = true;

	start_read();
	start_send();
}

void Websocket::onRead(error_code const &ec, string &&msg) {
	if(ec)
		COEUS_THROW(system_error{ec});

	this->onMessage(move(msg));

	start_read();
}

void Websocket::onWrite(error_code const &ec, size_t const) {
	if(ec)
		COEUS_THROW(system_error{ec});

	_sending = false;
	start_send();
}

void Websocket::onShutdown(error_code const &ec) {
	if(ec)
		COEUS_THROW(system_error{ec});

	get_lowest_layer(_sock).close();
}

auto Websocket::io() -> decltype(_io) {
	return _io;
}

auto Websocket::start_read() -> void {
	auto i = _buffs.emplace(_buffs.end());
	_sock.async_read(*i, [this, i](const auto &a, const auto &b) {
		this->onRead(a, string(static_cast<char const *>(i->data().data()), b));
		_buffs.erase(i);
	});
}

auto Websocket::start_send() -> void {
	if(!_reqs.empty()) {
		send_req(_reqs.front());
		_reqs.pop();
	}
}

auto Websocket::send_req(decltype(_reqs.front()) const &buf) -> void {
	_sending = true;
	_sock.async_write(net::const_buffer(buf.data(), buf.size()), bind(&Websocket::onWrite, this, placeholders::_1, placeholders::_2));
}
