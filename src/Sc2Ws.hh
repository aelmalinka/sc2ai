/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_SC2WS_INC
#	define SC2AI_SC2WS_INC

#	include "Websocket.hh"
#	include "s2clientprotocol/sc2api.pb.h"

	namespace Sc2Ai
	{
		using ::SC2APIProtocol::Request;
		using ::SC2APIProtocol::Response;

		class Sc2Ws :
			public Websocket
		{
			public:
				explicit Sc2Ws(
					decltype(std::declval<Sc2Ws>().io())
				);
				Sc2Ws(
					decltype(std::declval<Sc2Ws>().io()),
					std::string const &
				);
				virtual ~Sc2Ws();
				virtual void send(Request const &);
			protected:
				virtual void onMessage(std::string &&);
				virtual void onResponse(Response &&) = 0;
		};
	}

#endif
