/*	Copyright 2020 (c) Malina Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined SC2AI_URI_INC
#	define SC2AI_URI_INC

#	include "Exception.hh"
#	include <optional>

	namespace Sc2Ai
	{
		class Uri {
			public:
				class Authority {
					private:
						std::string _host;
						std::optional<std::string> _port;
						std::optional<std::string> _user;
					public:
						Authority();
						explicit operator std::string () const;
						auto Host() const -> decltype(_host) const &;
						auto Port() const -> decltype(*_port) const &;
						auto User() const -> decltype(*_user) const &;
						auto hasPort() const -> bool;
						auto hasUser() const -> bool;
						auto setHost(decltype(_host) const &) -> void;
						auto setPort(decltype(*_port) const &) -> void;
						auto setUser(decltype(*_user) const &) -> void;
				};
			private:
				std::string _scheme;
				std::string _path;
				std::optional<Authority> _authority;
				std::optional<std::string> _query;
				std::optional<std::string> _fragment;
			public:
				Uri();
				explicit Uri(std::string const &);
				explicit operator std::string () const;
				auto Scheme() const -> decltype(_scheme) const &;
				auto Path() const -> decltype(_path) const &;
				auto Auth() const -> decltype(*_authority) const &;
				auto Query() const -> decltype(*_query) const &;
				auto Fragment() const -> decltype(*_fragment) const &;
				auto hasAuthority() const -> bool;
				auto hasQuery() const -> bool;
				auto hasFragment() const -> bool;
				auto setScheme(decltype(_scheme) const &) -> void;
				auto setPath(decltype(_path) const &) -> void;
				auto setAuthority(decltype(*_authority) const &) -> void;
				auto setQuery(decltype(*_query) const &) -> void;
				auto setFragment(decltype(*_fragment) const &) -> void;
		};

		COEUS_ERROR_INFO(UriInfo, Uri);
	}

#endif
